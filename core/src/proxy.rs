use super::msg::NetEvent;
use super::msg::ProxyMsg;
use super::msg::Request;
use super::msg::Response;
use super::msg::WorkerMsg;
use bytes::Bytes;
use bytestring::ByteString;
use futures::channel::mpsc;
use futures::channel::oneshot;
use futures::future;
use futures::prelude::sink::SinkExt;
use futures::stream::Stream;
use futures::stream::StreamExt;
use log;
use std::cmp::Eq;
use std::collections::HashMap;
use std::fmt::Debug;
use std::future::Future;
use std::hash::Hash;
use std::marker::PhantomData;
use std::marker::Unpin;

#[derive(Debug)]
pub enum Error<WorkerId> {
    NoWorker,
    ProxyExit,
    WorkerDisconnected(WorkerId),
    Other(Box<dyn std::error::Error + Send + Sync + 'static>),
}

enum InnerMsg<WorkerId, MsgId> {
    Request(
        Bytes,                                           //请求数据
        oneshot::Sender<Result<Bytes, Error<WorkerId>>>, //回调
        Option<ByteString>,                              //指定处理的worker
    ),
    NetEvent((WorkerId, NetEvent<WorkerMsg<MsgId>>)),
    Exit,
}

#[derive(Clone)]
pub struct ProxyClient<WorkerId, MsgId> {
    sender: mpsc::UnboundedSender<InnerMsg<WorkerId, MsgId>>,
}

impl<WorkerId, MsgId> ProxyClient<WorkerId, MsgId> {
    fn new(
        sender: mpsc::UnboundedSender<InnerMsg<WorkerId, MsgId>>,
    ) -> ProxyClient<WorkerId, MsgId> {
        ProxyClient { sender: sender }
    }
    pub async fn call_remote(
        &self,
        request: Bytes,
        service_version: Option<ByteString>,
    ) -> Result<Bytes, Error<WorkerId>> {
        let mut inner_msg_sender = self.sender.clone();
        let (once_sender, once_receiver) = oneshot::channel();
        inner_msg_sender
            .send(InnerMsg::Request(request, once_sender, service_version))
            .await
            .map_err(|err| Error::Other(Box::new(err)))?;
        let response = once_receiver
            .await
            .map_err(|err| Error::Other(Box::new(err)))?;
        response
    }
}

pub struct Proxy<WorkerId, MsgId, MsgIdFn, Sender, SenderResult, Receiver>
where
    WorkerId: Hash + Eq + Clone + Debug + Send + Sync + 'static,
    Sender: Fn(WorkerId, ProxyMsg<MsgId>) -> SenderResult,
    SenderResult: Future<Output = Result<(), Box<dyn std::error::Error + Send + Sync + 'static>>>,
    Receiver: Stream<Item = (WorkerId, NetEvent<WorkerMsg<MsgId>>)> + Unpin,
    MsgId: Hash + Eq + Clone + Send + Sync + 'static,
    MsgIdFn: Fn() -> MsgId,
{
    send_msg: Sender,
    recv_msg: Receiver,
    msg_id_fn: MsgIdFn,
    inner_msg_sender: mpsc::UnboundedSender<InnerMsg<WorkerId, MsgId>>,
    inner_msg_receiver: mpsc::UnboundedReceiver<InnerMsg<WorkerId, MsgId>>,
    phantom1: PhantomData<WorkerId>,
    phantom2: PhantomData<SenderResult>,
}

impl<WorkerId, MsgId, MsgIdFn, Sender, SenderResult, Receiver>
    Proxy<WorkerId, MsgId, MsgIdFn, Sender, SenderResult, Receiver>
where
    WorkerId: Hash + Eq + Clone + Debug + Send + Sync + 'static,
    Sender: Fn(WorkerId, ProxyMsg<MsgId>) -> SenderResult,
    SenderResult: Future<Output = Result<(), Box<dyn std::error::Error + Send + Sync + 'static>>>,
    Receiver: Stream<Item = (WorkerId, NetEvent<WorkerMsg<MsgId>>)> + Unpin,
    MsgId: Hash + Eq + Clone + Send + Sync + 'static,
    MsgIdFn: Fn() -> MsgId,
{
    pub fn new(
        (send_msg, recv_msg): (Sender, Receiver),
        msg_id_fn: MsgIdFn,
    ) -> (
        Proxy<WorkerId, MsgId, MsgIdFn, Sender, SenderResult, Receiver>,
        ProxyClient<WorkerId, MsgId>,
    ) {
        let (inner_msg_sender, inner_msg_receiver) = mpsc::unbounded();
        (
            Proxy {
                send_msg: send_msg,
                recv_msg: recv_msg,
                msg_id_fn: msg_id_fn,
                inner_msg_sender: inner_msg_sender.clone(),
                inner_msg_receiver: inner_msg_receiver,
                phantom1: PhantomData,
                phantom2: PhantomData,
            },
            ProxyClient::new(inner_msg_sender),
        )
    }
    pub async fn serve(self) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
        let send_msg = self.send_msg;
        let mut recv_msg = self.recv_msg;
        let msg_id_fn = self.msg_id_fn;
        let inner_msg_sender = self.inner_msg_sender;
        let mut inner_msg_receiver = self.inner_msg_receiver;
        let mut wait_map: HashMap<
            MsgId,
            (oneshot::Sender<Result<Bytes, Error<WorkerId>>>, WorkerId),
        > = HashMap::new();
        let mut ability_map: HashMap<WorkerId, (u8, Option<ByteString>)> = HashMap::new();
        future::join(
            async {
                while let Some(msg) = inner_msg_receiver.next().await {
                    match msg {
                        InnerMsg::Request(request, callback, worker_version) => {
                            let task_id = msg_id_fn();
                            let lower_request = Request {
                                msg_id: task_id.clone(),
                                body: request,
                            };
                            let mut target_worker_id = None;
                            if let Some(worker_version) = worker_version {
                                for (worker_id, (_ability, version)) in &ability_map {
                                    if version.as_ref() == Some(&worker_version) {
                                        target_worker_id.replace(worker_id.clone());
                                        break;
                                    }
                                }
                            } else {
                                //如果没有指定version，就找version为空并且能力最优的worker来处理
                                let mut worker_info: Option<(&WorkerId, u8)> = None;
                                for (worker_id, (ability, version)) in &ability_map {
                                    if version.is_none() {
                                        if let Some((last_worker_id, last_ability)) =
                                            worker_info.as_mut()
                                        {
                                            if ability > last_ability {
                                                *last_worker_id = worker_id;
                                                *last_ability = *ability;
                                            }
                                        } else {
                                            worker_info.replace((worker_id, *ability));
                                        }
                                    }
                                }
                                target_worker_id = worker_info.map(|item| item.0.clone());
                            }
                            if let Some(worker_id) = target_worker_id {
                                wait_map.insert(task_id.clone(), (callback, worker_id.clone()));
                                if let Err(err) = send_msg(worker_id.clone(), lower_request).await {
                                    log::error!("{:?}", err);
                                    if let Some((callback, _worker_id)) = wait_map.remove(&task_id)
                                    {
                                        if let Err(err) = callback.send(Err(Error::Other(err))) {
                                            log::error!("{:?}", err);
                                        }
                                    }
                                }
                            } else {
                                //没有服务连接
                                if let Err(err) = callback.send(Err(Error::NoWorker)) {
                                    log::error!("{:?}", err);
                                }
                            }
                        }
                        InnerMsg::Exit => {
                            for (_task_id, (callback, _worker_id)) in wait_map {
                                if let Err(err) = callback.send(Err(Error::ProxyExit)) {
                                    log::error!("{:?}", err);
                                }
                            }
                            break;
                        }
                        InnerMsg::NetEvent((worker_id, event)) => match event {
                            NetEvent::Connected(service_version) => {
                                ability_map.insert(worker_id, (0, service_version));
                            }
                            NetEvent::Disconnected => {
                                ability_map.remove(&worker_id);
                                let mut task_ids = Vec::new();
                                for (task_id, (_callback, waiting_worker_id)) in wait_map.iter() {
                                    if worker_id == *waiting_worker_id {
                                        task_ids.push(task_id.clone());
                                    }
                                }
                                let task_len = task_ids.len();
                                for task_id in task_ids {
                                    if let Some((callback, worker_id)) = wait_map.remove(&task_id) {
                                        if let Err(err) =
                                            callback.send(Err(Error::WorkerDisconnected(worker_id)))
                                        {
                                            log::error!("{:?}", err);
                                        }
                                    }
                                }
                                shrink_map_if_necessary(&mut ability_map);
                                if 0 < task_len {
                                    shrink_map_if_necessary(&mut wait_map);
                                }
                            }
                            NetEvent::Message(worker_msg) => match worker_msg {
                                WorkerMsg::Response(Response { msg_id, body }) => {
                                    if let Some((callback, _worker_id)) = wait_map.remove(&msg_id) {
                                        if let Err(err) = callback.send(Ok(body)) {
                                            log::error!(
                                                "Response received, but receiver dropped, {:?}",
                                                err
                                            );
                                        }
                                        shrink_map_if_necessary(&mut wait_map);
                                    }
                                }
                                WorkerMsg::Ability(ability) => {
                                    ability_map.entry(worker_id).or_insert((0, None)).0 = ability;
                                }
                            },
                        },
                    }
                }
            },
            async {
                while let Some(item) = recv_msg.next().await {
                    if let Err(err) = inner_msg_sender
                        .clone()
                        .send(InnerMsg::NetEvent(item))
                        .await
                    {
                        log::error!("{:?}", err);
                    }
                }
                if let Err(err) = inner_msg_sender.clone().send(InnerMsg::Exit).await {
                    log::error!("{:?}", err);
                }
            },
        )
        .await;
        Ok(())
    }
}

#[inline]
fn shrink_map_if_necessary<K, V>(map: &mut HashMap<K, V>)
where
    K: Eq,
    K: Hash,
{
    let capacity = map.capacity();
    if 1024 < capacity && capacity > map.len() * 2 {
        map.shrink_to_fit();
    }
}
