use super::msg::NetEvent;
use super::msg::ProxyMsg;
use super::msg::Request;
use super::msg::Response;
use super::msg::WorkerMsg;
use bytes::Bytes;
use futures::channel::mpsc;
use futures::future;
use futures::lock::Mutex;
use futures::prelude::sink::SinkExt;
use futures::stream::Stream;
use futures::stream::StreamExt;
use log;
use std::collections::HashMap;
use std::fmt::Debug;
use std::future::Future;
use std::hash::Hash;
use std::marker::PhantomData;
use std::marker::Unpin;
use std::pin::Pin;
use std::sync::Arc;

enum InnerMsg<CallerId, MsgId> {
    Ability(u8),
    NetEvent((CallerId, NetEvent<ProxyMsg<MsgId>>)),
}

pub struct Worker<
    CallerId,
    MsgId,
    Sender,
    SenderResult,
    Receiver,
    Handler,
    HandlerResult,
    Executor,
    Abilities,
> where
    CallerId: Hash + Eq + Clone + Debug + Send + Sync + 'static,
    Sender: Fn(CallerId, WorkerMsg<MsgId>) -> SenderResult,
    Receiver: Stream<Item = (CallerId, NetEvent<ProxyMsg<MsgId>>)> + Unpin,
    SenderResult: Future<Output = Result<(), Box<dyn std::error::Error + Send + Sync + 'static>>>,
    Handler: Fn(Bytes) -> HandlerResult,
    Abilities: Stream<Item = u8> + Unpin,
    HandlerResult: Future<Output = Bytes>,
{
    send_msg: Sender,
    recv_msg: Receiver,
    handler: Handler,
    executor: Executor,
    abilities: Abilities,
    inner_msg_sender: mpsc::UnboundedSender<InnerMsg<CallerId, MsgId>>,
    inner_msg_receiver: mpsc::UnboundedReceiver<InnerMsg<CallerId, MsgId>>,
    phantom1: PhantomData<SenderResult>,
}

impl<
        CallerId,
        MsgId,
        Sender,
        SenderResult,
        Receiver,
        Handler,
        HandlerResult,
        Executor,
        Abilities,
    >
    Worker<
        CallerId,
        MsgId,
        Sender,
        SenderResult,
        Receiver,
        Handler,
        HandlerResult,
        Executor,
        Abilities,
    >
where
    CallerId: Hash + Eq + Clone + Debug + Send + Sync + 'static,
    Sender: Fn(CallerId, WorkerMsg<MsgId>) -> SenderResult + Sync + Send + 'static,
    Receiver: Stream<Item = (CallerId, NetEvent<ProxyMsg<MsgId>>)> + Unpin,
    SenderResult:
        Future<Output = Result<(), Box<dyn std::error::Error + Send + Sync + 'static>>> + Send,
    MsgId: Send + 'static,
    Handler: Fn(Bytes) -> HandlerResult + Send + Sync + 'static,
    Executor: Fn(Pin<Box<dyn Future<Output = ()> + Send + 'static>>),
    Abilities: Stream<Item = u8> + Unpin,
    HandlerResult: Future<Output = Bytes> + Send,
{
    pub fn new(
        (send_msg, recv_msg): (Sender, Receiver),
        handler: Handler,
        executor: Executor,
        abilities: Abilities,
    ) -> Worker<
        CallerId,
        MsgId,
        Sender,
        SenderResult,
        Receiver,
        Handler,
        HandlerResult,
        Executor,
        Abilities,
    > {
        let (msg_sender, msg_receiver) = mpsc::unbounded();
        Worker {
            send_msg: send_msg,
            recv_msg: recv_msg,
            handler: handler,
            executor: executor,
            abilities: abilities,
            inner_msg_sender: msg_sender,
            inner_msg_receiver: msg_receiver,
            phantom1: PhantomData,
        }
    }
    pub async fn serve(self) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
        let send_msg = Arc::new(self.send_msg);
        let mut recv_msg = self.recv_msg;
        let inner_msg_sender = self.inner_msg_sender;
        let mut inner_msg_receiver = self.inner_msg_receiver;
        let handler = Arc::new(self.handler);
        let executor = self.executor;
        let mut abilities = self.abilities;
        let mut caller_map: HashMap<CallerId, Arc<Mutex<()>>> = HashMap::new();
        future::join3(
            async {
                while let Some(msg) = inner_msg_receiver.next().await {
                    match msg {
                        InnerMsg::Ability(ability) => {
                            for (caller_id, mutex) in &caller_map {
                                let send_msg = send_msg.clone();
                                let caller_id = caller_id.clone();
                                let mutex = mutex.clone();
                                executor(Box::pin(async move {
                                    let lock = mutex.lock().await;
                                    if let Err(err) =
                                        send_msg(caller_id, WorkerMsg::Ability(ability)).await
                                    {
                                        log::error!("{:?}", err);
                                    }
                                    drop(lock);
                                }));
                            }
                        }
                        InnerMsg::NetEvent((caller_id, event)) => match event {
                            NetEvent::Connected(_) => {
                                caller_map.insert(caller_id, Arc::new(Mutex::new(())));
                            }
                            NetEvent::Disconnected => {
                                caller_map.remove(&caller_id);
                                shrink_map_if_necessary(&mut caller_map);
                            }
                            NetEvent::Message(proxy_msg) => {
                                let Request { msg_id, body } = proxy_msg;
                                let handler = handler.clone();
                                let send_msg = send_msg.clone();
                                if let Some(mutex) =
                                    caller_map.get(&caller_id).map(|item| item.clone())
                                {
                                    executor(Box::pin(async move {
                                        let response = handler(body).await;
                                        let lock = mutex.lock().await;
                                        if let Err(err) = send_msg(
                                            caller_id,
                                            WorkerMsg::Response(Response {
                                                msg_id: msg_id,
                                                body: response,
                                            }),
                                        )
                                        .await
                                        {
                                            log::error!("{:?}", err);
                                        }
                                        drop(lock);
                                    }));
                                }
                            }
                        },
                    }
                }
            },
            async {
                while let Some(item) = recv_msg.next().await {
                    if let Err(err) = inner_msg_sender
                        .clone()
                        .send(InnerMsg::NetEvent(item))
                        .await
                    {
                        log::error!("{:?}", err);
                    }
                }
            },
            async {
                while let Some(ability) = abilities.next().await {
                    if let Err(err) = inner_msg_sender
                        .clone()
                        .send(InnerMsg::Ability(ability))
                        .await
                    {
                        log::error!("{:?}", err);
                    }
                }
            },
        )
        .await;
        Ok(())
    }
}

#[inline]
fn shrink_map_if_necessary<K, V>(map: &mut HashMap<K, V>)
where
    K: Eq,
    K: Hash,
{
    let capacity = map.capacity();
    if 1024 < capacity && capacity > map.len() * 2 {
        map.shrink_to_fit();
    }
}
