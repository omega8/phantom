use bytes::Bytes;
use bytestring::ByteString;

#[derive(Debug)]
pub enum NetEvent<Msg> {
    Message(Msg),
    Connected(Option<ByteString>),
    Disconnected,
}

#[derive(Debug)]
pub struct Request<MsgId> {
    pub msg_id: MsgId,
    pub body: Bytes,
}

#[derive(Debug)]
pub struct Response<MsgId> {
    pub msg_id: MsgId,
    pub body: Bytes,
}

pub type ProxyMsg<MsgId> = Request<MsgId>;

#[derive(Debug)]
pub enum WorkerMsg<MsgId> {
    Response(Response<MsgId>),
    Ability(u8),
}
