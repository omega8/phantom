use super::msg::ProxyMsg;
use super::msg::Request;
use super::msg::Response;
use super::msg::WorkerMsg;
use super::tuple::TupleMany;
use super::tuple::TupleManyStruct;
use super::tuple::TupleManyTrait;
use bytes::Bytes;
use bytestring::ByteString;
use integer_encoding::VarInt;
use std::convert::TryInto;

fn read_be_u128(mut input: Bytes) -> Result<(u128, Bytes), ByteString> {
    let u128_len = std::mem::size_of::<u128>();
    if input.len() < u128_len {
        return Err(ByteString::from_static("输入数据长度不够"));
    } else {
        let rest = input.split_off(u128_len);
        return Ok((
            u128::from_be_bytes(
                input
                    .as_ref()
                    .try_into()
                    .map_err(|err| -> ByteString { format!("{:?}", err).into() })?,
            ),
            rest,
        ));
    }
}

pub trait Encoder {
    type MsgId;
    type Error;
    fn encode_worker_msg(&self, msg: WorkerMsg<Self::MsgId>) -> Vec<u8>;
    fn decode_worker_msg(&self, raw: Bytes) -> Result<WorkerMsg<Self::MsgId>, Self::Error>;
    fn encode_proxy_msg(&self, msg: ProxyMsg<Self::MsgId>) -> Vec<u8>;
    fn decode_proxy_msg(&self, raw: Bytes) -> Result<ProxyMsg<Self::MsgId>, Self::Error>;
}

pub struct MsgEncoder {}

impl MsgEncoder {
    pub fn new() -> Self {
        Self {}
    }
}

impl Encoder for MsgEncoder {
    type MsgId = u128;
    type Error = ByteString;

    fn encode_worker_msg(&self, msg: WorkerMsg<Self::MsgId>) -> Vec<u8> {
        match msg {
            WorkerMsg::Response(Response { msg_id, body }) => {
                let msg_id = msg_id.to_be_bytes();
                let msg_id_len = msg_id.len();
                let body_len = body.len();
                let total_len = 1 + msg_id_len + body_len;
                let mut new_msg = vec![1; total_len];
                (&mut new_msg[1..1 + msg_id_len]).copy_from_slice(&msg_id);
                (&mut new_msg[1 + msg_id_len..total_len]).copy_from_slice(&body);
                return new_msg;
            }
            WorkerMsg::Ability(ability) => {
                let mut new_msg = Vec::with_capacity(2);
                new_msg.push(2);
                new_msg.push(ability);
                return new_msg;
            }
        }
    }
    fn decode_worker_msg(&self, mut message: Bytes) -> Result<WorkerMsg<Self::MsgId>, Self::Error> {
        if 1 > message.len() {
            return Err(ByteString::from_static("输入数据长度不够"));
        } else {
            let msg_type = message[0];
            if 1 == msg_type {
                return read_be_u128(message.split_off(1))
                    .map(|(msg_id, body)| WorkerMsg::Response(Response { msg_id, body: body }));
            } else if 2 == msg_type {
                if 2 == message.len() {
                    return Ok(WorkerMsg::Ability(message[1]));
                } else {
                    return Err(ByteString::from_static("能力上报消息的消息内容长度异常"));
                }
            } else {
                return Err(format!("未知的消息类型, msg_type: {}", msg_type).into());
            }
        }
    }
    fn encode_proxy_msg(&self, msg: ProxyMsg<Self::MsgId>) -> Vec<u8> {
        let Request { msg_id, body } = msg;
        let msg_id = msg_id.to_be_bytes();
        let msg_id_len = msg_id.len();
        let body_len = body.len();
        let total_len = msg_id_len + body_len;
        let mut new_msg = vec![0; total_len];
        (&mut new_msg[0..msg_id_len]).copy_from_slice(&msg_id);
        (&mut new_msg[msg_id_len..total_len]).copy_from_slice(&body);
        return new_msg;
    }
    fn decode_proxy_msg(&self, message: Bytes) -> Result<ProxyMsg<Self::MsgId>, Self::Error> {
        return read_be_u128(message).map(|(msg_id, body)| Request { msg_id, body: body });
    }
}

/// This is the max required bytes to encode a u64 using the varint encoding scheme.
/// It is size 10=ceil(64/7)
const MAX_ENCODED_SIZE: usize = 10;

/// Encode a message, returning the bytes that must be sent before the message.
/// A buffer is used to avoid heap allocation.
fn encode_size<'a>(message: &[u8], buf: &'a mut [u8; MAX_ENCODED_SIZE]) -> &'a [u8] {
    let varint_size = message.len().encode_var(buf);
    &buf[..varint_size]
}

pub struct ChunkEncoder {}

impl ChunkEncoder {
    pub fn new() -> Self {
        Self {}
    }
}

impl ChunkEncoder {
    pub fn encode(&self, chunks: &[&[u8]], output: Option<Vec<u8>>) -> Vec<u8> {
        if chunks.is_empty() {
            return Vec::new();
        }
        let count = chunks.len();
        let mut output = output.unwrap_or_else(|| {
            let max_len = (count - 1) * MAX_ENCODED_SIZE
                + chunks.iter().map(|chunk| chunk.len()).sum::<usize>();
            Vec::with_capacity(max_len)
        });
        let mut buf = [0; MAX_ENCODED_SIZE];
        for (index, chunk) in chunks.iter().enumerate() {
            if index + 1 < count {
                output.extend_from_slice(&*encode_size(chunk, &mut buf));
            }
            output.extend_from_slice(chunk);
        }
        return output;
    }
    pub fn decode<const N: usize>(
        &self,
        mut chunk: Bytes,
    ) -> Result<TupleMany<N, Bytes>, ByteString>
    where
        TupleManyStruct<N>: TupleManyTrait<Bytes>,
    {
        let mut count: usize = N;
        let mut chunks: Vec<Bytes> = Vec::with_capacity(count);
        while 1 < count {
            let (size, cost) = usize::decode_var(&chunk)
                .ok_or_else(|| ByteString::from_static("输入数据长度不够"))?;
            let mut remain = chunk.split_off(cost);
            if remain.len() < size {
                return Err(ByteString::from_static("输入数据长度不够"));
            } else {
                chunk = remain.split_off(size);
                chunks.push(remain);
                count -= 1;
            }
        }
        chunks.push(chunk);
        return TupleManyStruct::try_from_iter(chunks.into_iter())
            .map_err(|_| ByteString::from_static("数据元素个数不正确"));
    }
}
