use super::new_msg_id;
use super::proxy::Client;
use crate::utils::encrypt;
use async_trait::async_trait;
use bytestring::ByteString;
use futures::Stream;
use futures::StreamExt;
use log;
use phantom_core::encoder::Encoder;
use phantom_core::encoder::MsgEncoder;
use phantom_core::msg;
use phantom_core::msg::ProxyMsg;
use phantom_core::proxy::Proxy;
pub use phantom_network_adapter::tokio::ConnectInfo;
use phantom_network_adapter::tokio::TokioClientAdapter;
use phantom_network_adapter::tokio_rustls::TokioRustlsClientAdapter;
use phantom_network_adapter::Channel;
use phantom_network_adapter::ClientAdapter;
use phantom_network_adapter::Handshake;
use phantom_network_adapter::MsgRead;
use phantom_network_adapter::MsgSender;
use phantom_network_adapter::MsgWrite;
use phantom_network_adapter::ServerMgr;
use std::future::Future;

/**
 * 客户端Proxy握手逻辑
 */
#[derive(Clone)]
pub struct ClientProxyHandshake {
    pub aes_key: [u8; 32],
}

#[async_trait]
impl Handshake for ClientProxyHandshake {
    type Output = Option<ByteString>;
    type Error = phantom_network_adapter::Error;
    async fn handshake(
        &self,
        reader: &mut (dyn MsgRead<Error = std::io::Error> + Send),
        writer: &mut (dyn MsgWrite<Error = std::io::Error> + Send),
    ) -> Result<Self::Output, Self::Error> {
        let first_msg = reader
            .read_msg()
            .await
            .map_err(phantom_network_adapter::Error::IoError)?;
        if let Some(first_msg) = first_msg {
            let mut plain =
                encrypt::decrypt_by_aes_256(&first_msg, &self.aes_key).map_err(|err| {
                    log::error!("Aes decrypt first message of handshake failed: {:?}", err);
                    phantom_network_adapter::Error::HandshakeError(ByteString::from_static(
                        "Aes decrypt first message of handshake failed",
                    ))
                })?;
            let plain_len = plain.len();
            let random_len = 512;
            if random_len <= plain_len {
                let data_len = plain_len - random_len;
                //后面部分是随机数据，写回去
                writer
                    .write_msg(&plain[data_len..])
                    .await
                    .map_err(phantom_network_adapter::Error::IoError)?;
                plain.truncate(data_len);
                if plain.is_empty() {
                    return Ok(None);
                } else {
                    let service_version = String::from_utf8(plain).map_err(|err| {
                        log::error!("Service version is not a UTF-8 string: {:?}", err);
                        phantom_network_adapter::Error::HandshakeError(ByteString::from_static(
                            "Service version is not a UTF-8 string",
                        ))
                    })?;
                    return Ok(Some(service_version.into()));
                }
            } else {
                return Err(phantom_network_adapter::Error::HandshakeError(
                    ByteString::from_static("First message from server is invalid"),
                ));
            }
        } else {
            return Err(phantom_network_adapter::Error::HandshakeError(
                ByteString::from_static("First message is none"),
            ));
        }
    }
}

fn build_rpc_client(
    msg_sender: impl MsgSender<Channel = Channel> + 'static,
    msg_receiver: impl Stream<Item = (Channel, phantom_network_adapter::NetEvent)> + Unpin + 'static,
    aes_key: Option<[u8; 32]>,
) -> (
    Client,
    impl Future<Output = Result<(), Box<dyn std::error::Error + Send + Sync>>> + 'static,
) {
    let (proxy, client) = Proxy::new(
        (
            move |channel: Channel, proxy_msg: ProxyMsg<<MsgEncoder as Encoder>::MsgId>| {
                let msg_sender = msg_sender.clone();
                Box::pin(async move {
                    let encoder = MsgEncoder::new();
                    let raw_msg = encoder.encode_proxy_msg(proxy_msg);
                    msg_sender.send_msg(&channel, &raw_msg).await
                })
            },
            msg_receiver.filter_map(move |(channel, event)| {
                return Box::pin(async move {
                    match event {
                        phantom_network_adapter::NetEvent::Connected(service_version) => {
                            Some((channel, msg::NetEvent::Connected(service_version)))
                        }
                        phantom_network_adapter::NetEvent::Message(raw_msg) => {
                            let encoder = MsgEncoder::new();
                            match encoder.decode_worker_msg(raw_msg.into()) {
                                Ok(worker_msg) => {
                                    Some((channel, msg::NetEvent::Message(worker_msg)))
                                }
                                Err(err) => {
                                    log::error!("收到异常数据包: {:?}", err);
                                    None
                                }
                            }
                        }
                        phantom_network_adapter::NetEvent::Disconnected => {
                            Some((channel, msg::NetEvent::Disconnected))
                        }
                    }
                });
            }),
        ),
        new_msg_id,
    );

    return (Client::new(client, aes_key), proxy.serve());
}

pub async fn build_common_rpc_client(
    aes_key: [u8; 32],
    encrypt_message: bool,
) -> (
    Client,
    ServerMgr<phantom_network_adapter::tokio::ConnectInfo>,
    impl Future<Output = Result<(), Box<dyn std::error::Error + Send + Sync>>> + Send + 'static,
) {
    let client_proxy_handshake = ClientProxyHandshake { aes_key: aes_key };
    let (msg_sender, msg_receiver, server_mgr) =
        TokioClientAdapter::build_channel(client_proxy_handshake).await;
    let aes_key_opt = if encrypt_message { Some(aes_key) } else { None };
    let (client, service) = build_rpc_client(msg_sender, msg_receiver, aes_key_opt);
    return (client, server_mgr, service);
}

pub async fn build_tls_rpc_client(
    aes_key: [u8; 32],
    encrypt_message: bool,
) -> (
    Client,
    ServerMgr<phantom_network_adapter::tokio_rustls::ConnectInfo>,
    impl Future<Output = Result<(), Box<dyn std::error::Error + Send + Sync>>> + Send + 'static,
) {
    let client_proxy_handshake = ClientProxyHandshake { aes_key: aes_key };
    let (msg_sender, msg_receiver, server_mgr) =
        TokioRustlsClientAdapter::build_channel(client_proxy_handshake).await;
    let aes_key_opt = if encrypt_message { Some(aes_key) } else { None };
    let (client, service) = build_rpc_client(msg_sender, msg_receiver, aes_key_opt);
    return (client, server_mgr, service);
}
