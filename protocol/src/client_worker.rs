use super::worker::build_worker_future;
use super::ConnectParams;
use super::Request;
use super::Response;
use crate::utils::encrypt;
use async_trait::async_trait;
use bytestring::ByteString;
use futures::stream::Stream;
use phantom_network_adapter::tokio::TokioClientAdapter;
use phantom_network_adapter::tokio_rustls::TokioRustlsClientAdapter;
use phantom_network_adapter::ClientAdapter;
use phantom_network_adapter::Handshake;
use phantom_network_adapter::MsgRead;
use phantom_network_adapter::MsgWrite;
use phantom_network_adapter::ServerMgr;
use std::future::Future;

/**
 * 客户端Worker握手逻辑
 * 服务端先发送包含随机数据和服务版本的密文，客户端返回明文随机数据
 */
#[derive(Clone)]
pub struct ClientWorkerHandshake {
    pub aes_key: [u8; 32],
    pub service_version: Option<ByteString>,
}

#[async_trait]
impl Handshake for ClientWorkerHandshake {
    type Output = Option<ByteString>;
    type Error = phantom_network_adapter::Error;
    async fn handshake(
        &self,
        reader: &mut (dyn MsgRead<Error = std::io::Error> + Send),
        writer: &mut (dyn MsgWrite<Error = std::io::Error> + Send),
    ) -> Result<Self::Output, Self::Error> {
        let first_msg = reader
            .read_msg()
            .await
            .map_err(phantom_network_adapter::Error::IoError)?;
        if let Some(first_msg) = first_msg {
            let mut data = Vec::new();
            if let Some(service_version) = self.service_version.as_ref() {
                data.extend(service_version.as_bytes());
            }
            data.extend(first_msg);
            match encrypt::encrypt_by_aes_256(&data, &self.aes_key) {
                Ok(cipher) => {
                    writer
                        .write_msg(&cipher)
                        .await
                        .map_err(phantom_network_adapter::Error::IoError)?;
                    return Ok(None);
                }
                Err(err) => {
                    log::error!("Aes encrypt random data of handshake failed: {:?}", err);
                    return Err(phantom_network_adapter::Error::HandshakeError(
                        ByteString::from_static("Aes encrypt random data of handshake failed"),
                    ));
                }
            }
        } else {
            return Err(phantom_network_adapter::Error::HandshakeError(
                ByteString::from_static("First message is none"),
            ));
        }
    }
}

async fn build_tls_worker_service<Handler, HandlerResult, AbilityStream>(
    handler: Handler,
    client_worker_handshake: ClientWorkerHandshake,
    encrypt_message: bool,
    ability_stream: AbilityStream,
) -> (
    ServerMgr<phantom_network_adapter::tokio_rustls::ConnectInfo>,
    impl Future<Output = Result<(), Box<dyn std::error::Error + Send + Sync>>> + Send + 'static,
)
where
    Handler: Fn(Request) -> HandlerResult + Send + Sync + 'static,
    HandlerResult: Future<Output = Result<Response, ByteString>> + Send + 'static,
    AbilityStream: Stream<Item = u8> + Send + Unpin + 'static,
{
    let aes_key = client_worker_handshake.aes_key;
    let (worker_send_msg, worker_read_stream, server_mgr) =
        TokioRustlsClientAdapter::build_channel(client_worker_handshake).await;
    let aes_key_opt = if encrypt_message { Some(aes_key) } else { None };
    return (
        server_mgr,
        build_worker_future(
            handler,
            worker_send_msg,
            worker_read_stream,
            aes_key_opt,
            ability_stream,
        ),
    );
}

async fn build_worker_service<Handler, HandlerResult, AbilityStream>(
    handler: Handler,
    client_worker_handshake: ClientWorkerHandshake,
    encrypt_message: bool,
    ability_stream: AbilityStream,
) -> (
    ServerMgr<phantom_network_adapter::tokio::ConnectInfo>,
    impl Future<Output = Result<(), Box<dyn std::error::Error + Send + Sync>>> + Send + 'static,
)
where
    Handler: Fn(Request) -> HandlerResult + Send + Sync + 'static,
    HandlerResult: Future<Output = Result<Response, ByteString>> + Send + 'static,
    AbilityStream: Stream<Item = u8> + Send + Unpin + 'static,
{
    let aes_key = client_worker_handshake.aes_key;
    let (worker_send_msg, worker_read_stream, server_mgr) =
        TokioClientAdapter::build_channel(client_worker_handshake).await;
    let aes_key_opt = if encrypt_message { Some(aes_key) } else { None };
    return (
        server_mgr,
        build_worker_future(
            handler,
            worker_send_msg,
            worker_read_stream,
            aes_key_opt,
            ability_stream,
        ),
    );
}

pub async fn start_worker_service<Handler, HandlerResult, AbilityStream>(
    connect_params: ConnectParams,
    handler: Handler,
    aes_key: [u8; 32],
    service_version: Option<ByteString>,
    encrypt_message: bool,
    ability_stream: AbilityStream,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>>
where
    Handler: Fn(Request) -> HandlerResult + Send + Sync + 'static,
    HandlerResult: Future<Output = Result<Response, ByteString>> + Send + 'static,
    AbilityStream: Stream<Item = u8> + Send + Unpin + 'static,
{
    let client_worker_handshake = ClientWorkerHandshake {
        aes_key: aes_key,
        service_version: service_version,
    };
    match connect_params {
        ConnectParams::Tls(connect_params) => {
            let (server_mgr, service) = build_tls_worker_service(
                handler,
                client_worker_handshake,
                encrypt_message,
                ability_stream,
            )
            .await;
            server_mgr.add_server(connect_params).await;
            return service.await;
        }
        ConnectParams::Common(connect_params) => {
            let (server_mgr, service) = build_worker_service(
                handler,
                client_worker_handshake,
                encrypt_message,
                ability_stream,
            )
            .await;
            server_mgr.add_server(connect_params).await;
            return service.await;
        }
    }
}
