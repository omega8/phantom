use super::utils::encrypt;
use super::Request;
use super::Response;
use bytes::Bytes;
use bytestring::ByteString;
use phantom_core::encoder::ChunkEncoder;
use std::collections::HashMap;
use std::convert::TryFrom;

fn pkg_metadata<S, T>(
    path: &[u8],
    ip_addr: &[u8],
    session: &Option<S>,
    versions: &Option<T>,
    headers: &Option<T>,
    output: Option<Vec<u8>>,
) -> Vec<u8>
where
    S: AsRef<[u8]>,
    T: AsRef<[u8]>,
{
    let encoder = ChunkEncoder::new();
    let session_bytes = session
        .as_ref()
        .map(|session| session.as_ref())
        .unwrap_or(b"");
    let versions = versions
        .as_ref()
        .map(|versions| versions.as_ref())
        .unwrap_or(b"");
    let headers = headers
        .as_ref()
        .map(|headers| headers.as_ref())
        .unwrap_or(b"");
    encoder.encode(&[path, ip_addr, session_bytes, versions, headers], output)
}

pub fn encode_request(
    request: &Request,
    aes_key: Option<[u8; 32]>,
    output: Option<Vec<u8>>,
) -> Result<Vec<u8>, Box<dyn std::error::Error + Send + Sync>> {
    let versions = if let Some(versions) = request.versions.as_ref() {
        let versions = serde_json::to_vec::<HashMap<String, String>>(versions)?;
        Some(versions)
    } else {
        None
    };
    let headers = if let Some(headers) = request.headers.as_ref() {
        let headers = serde_json::to_vec::<HashMap<String, Option<String>>>(headers)?;
        Some(headers)
    } else {
        None
    };
    let metadata = pkg_metadata(
        request.uri.as_bytes(),
        request.ip.as_bytes(),
        &request.session,
        &versions,
        &headers,
        None,
    );
    let encoder = ChunkEncoder::new();
    let mut req = encoder.encode(&[&metadata, &request.body], None);
    if let Some(aes_key) = aes_key {
        req = encrypt::encrypt_by_aes_256(&req, &aes_key).map_err(
            |err| -> Box<dyn std::error::Error + Send + Sync> {
                log::error!("Aes encrypt request message failed: {:?}", err);
                return "Aes encrypt request message failed".into();
            },
        )?;
    }
    return Ok(if let Some(mut output) = output {
        output.append(&mut req);
        output
    } else {
        req
    });
}

pub fn encode_response(
    response: &Response,
    aes_key: Option<[u8; 32]>,
    output: Option<Vec<u8>>,
) -> Result<Vec<u8>, Box<dyn std::error::Error + Send + Sync>> {
    let encoder = ChunkEncoder::new();
    let mut resp = if let Some(session) = response.session.as_ref() {
        encoder.encode(&[session.as_bytes(), &response.body], None)
    } else {
        encoder.encode(&[b"", &response.body], None)
    };
    if let Some(aes_key) = aes_key {
        resp = encrypt::encrypt_by_aes_256(&resp, &aes_key).map_err(
            |err| -> Box<dyn std::error::Error + Send + Sync> {
                log::error!("Aes encrypt response message failed: {:?}", err);
                return "Aes encrypt response message failed".into();
            },
        )?;
    }
    return Ok(if let Some(mut output) = output {
        output.append(&mut resp);
        output
    } else {
        resp
    });
}

pub fn decode_request(
    mut req: Bytes,
    aes_key: Option<[u8; 32]>,
) -> Result<Request, Box<dyn std::error::Error + Send + Sync>> {
    if let Some(aes_key) = aes_key {
        req = encrypt::decrypt_by_aes_256(&req, &aes_key)
            .map_err(|err| -> Box<dyn std::error::Error + Send + Sync> {
                log::error!("Decrypt request failed: {:?}", err);
                return "Decrypt request failed".into();
            })?
            .into();
    }
    let encoder = ChunkEncoder::new();
    let (meta, body) = encoder.decode::<2>(req.into()).map_err(
        |err| -> Box<dyn std::error::Error + Send + Sync> {
            log::error!("Decode request failed: {:?}", err);
            return "Request format invalid!".into();
        },
    )?;
    let (uri, ip, session, versions, headers) =
        encoder
            .decode::<5>(meta)
            .map_err(|err| -> Box<dyn std::error::Error + Send + Sync> {
                log::error!("Decode metadata failed: {:?}", err);
                return "Request format invalid!".into();
            })?;
    let uri = ByteString::try_from(uri)?;
    let ip = ByteString::try_from(ip)?;
    let session = if session.is_empty() {
        None
    } else {
        Some(ByteString::try_from(session)?)
    };
    let versions = if versions.is_empty() {
        None
    } else {
        let versions = serde_json::from_slice::<HashMap<String, String>>(&versions)?;
        Some(versions)
    };
    let headers = if headers.is_empty() {
        None
    } else {
        let headers = serde_json::from_slice::<HashMap<String, Option<String>>>(&headers)?;
        Some(headers)
    };
    return Ok(Request {
        uri: uri,
        ip: ip,
        session: session,
        versions: versions,
        headers: headers,
        body: body,
    });
}

pub fn decode_response(
    mut resp: Bytes,
    aes_key: Option<[u8; 32]>,
) -> Result<Response, Box<dyn std::error::Error + Send + Sync>> {
    if let Some(aes_key) = aes_key {
        resp = encrypt::decrypt_by_aes_256(&resp, &aes_key)
            .map_err(|err| -> Box<dyn std::error::Error + Send + Sync> {
                log::error!("Decrypt response failed: {:?}", err);
                return "Decrypt response failed".into();
            })?
            .into();
    }
    let encoder = ChunkEncoder::new();
    let (session, body) = encoder.decode::<2>(resp.into()).map_err(
        |err| -> Box<dyn std::error::Error + Send + Sync> {
            log::error!("Decode response failed: {:?}", err);
            return "Decode response failed".into();
        },
    )?;
    return Ok(Response {
        session: if session.is_empty() {
            None
        } else {
            Some(ByteString::try_from(session)?)
        },
        body,
    });
}
