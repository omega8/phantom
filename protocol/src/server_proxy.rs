use super::new_msg_id;
use super::BindInfo;
use crate::proxy::Client;
use crate::utils::encrypt;
use async_trait::async_trait;
use bytestring::ByteString;
use futures::stream::StreamExt;
use log;
use phantom_core::encoder::Encoder;
use phantom_core::encoder::MsgEncoder;
use phantom_core::msg;
use phantom_core::msg::ProxyMsg;
use phantom_core::proxy::Proxy;
use phantom_network_adapter::tokio::TokioServerAdapter;
use phantom_network_adapter::tokio_rustls::TokioRustlsServerAdapter;
use phantom_network_adapter::Channel;
use phantom_network_adapter::Handshake;
use phantom_network_adapter::MsgRead;
use phantom_network_adapter::MsgSender;
use phantom_network_adapter::MsgWrite;
use phantom_network_adapter::ServerAdapter;

/**
 * 服务端Proxy握手逻辑
 * 服务端先发送明文随机数据，客户端返回包含随机数据和服务版本的密文
 */
pub struct ServerProxyHandshake {
    pub aes_key: [u8; 32],
}

#[async_trait]
impl Handshake for ServerProxyHandshake {
    type Output = Option<ByteString>;
    type Error = phantom_network_adapter::Error;
    async fn handshake(
        &self,
        reader: &mut (dyn MsgRead<Error = std::io::Error> + Send),
        writer: &mut (dyn MsgWrite<Error = std::io::Error> + Send),
    ) -> Result<Option<ByteString>, Self::Error> {
        let mut random_data: [u8; 512] = [0; 512];
        encrypt::fill_random_bytes(&mut random_data);
        writer
            .write_msg(&random_data)
            .await
            .map_err(phantom_network_adapter::Error::IoError)?;
        let first_msg = reader
            .read_msg()
            .await
            .map_err(phantom_network_adapter::Error::IoError)?;
        if let Some(first_msg) = first_msg {
            let mut plain =
                encrypt::decrypt_by_aes_256(&first_msg, &self.aes_key).map_err(|err| {
                    log::error!("Aes decrypt first message of handshake failed: {:?}", err);
                    phantom_network_adapter::Error::HandshakeError(ByteString::from_static(
                        "Aes decrypt first message of handshake failed",
                    ))
                })?;
            if plain.ends_with(&random_data) {
                let data_len = plain.len() - random_data.len();
                plain.truncate(data_len);
                if plain.is_empty() {
                    return Ok(None);
                } else {
                    let service_version = String::from_utf8(plain).map_err(|err| {
                        log::error!("Service name is not a UTF-8 string: {:?}", err);
                        phantom_network_adapter::Error::HandshakeError(ByteString::from_static(
                            "Service name is not a UTF-8 string",
                        ))
                    })?;
                    return Ok(Some(service_version.into()));
                }
            } else {
                return Err(phantom_network_adapter::Error::HandshakeError(
                    ByteString::from_static("Cipher from client is invalid"),
                ));
            }
        } else {
            return Err(phantom_network_adapter::Error::HandshakeError(
                ByteString::from_static("First message is none"),
            ));
        }
    }
}

async fn start_tls_proxy_service(
    bind_info: phantom_network_adapter::tokio_rustls::BindInfo,
    aes_key: [u8; 32],
    encrypt_message: bool,
) -> Result<Client, ByteString> {
    let server_proxy_handshake: ServerProxyHandshake = ServerProxyHandshake { aes_key: aes_key };
    let (sender, receiver) =
        TokioRustlsServerAdapter::build_channel(bind_info, server_proxy_handshake)
            .await
            .map_err(|err| -> ByteString {
                log::error!("{:?}", err);
                ByteString::from_static("启动代理服务失败")
            })?;
    let (proxy, client) = Proxy::new(
        (
            move |channel: Channel, proxy_msg: ProxyMsg<<MsgEncoder as Encoder>::MsgId>| {
                let sender = sender.clone();
                Box::pin(async move {
                    let encoder = MsgEncoder::new();
                    let raw_msg = encoder.encode_proxy_msg(proxy_msg);
                    sender.send_msg(&channel, &raw_msg).await
                })
            },
            receiver.filter_map(|(channel, msg)| {
                let ret = match msg {
                    phantom_network_adapter::NetEvent::Connected(service_version) => {
                        Some((channel, msg::NetEvent::Connected(service_version)))
                    }
                    phantom_network_adapter::NetEvent::Message(raw_msg) => {
                        let encoder = MsgEncoder::new();
                        match encoder.decode_worker_msg(raw_msg) {
                            Ok(worker_msg) => Some((channel, msg::NetEvent::Message(worker_msg))),
                            Err(err) => {
                                log::error!("收到异常数据包: {:?}", err);
                                None
                            }
                        }
                    }
                    phantom_network_adapter::NetEvent::Disconnected => {
                        Some((channel, msg::NetEvent::Disconnected))
                    }
                };
                return Box::pin(async { ret });
            }),
        ),
        new_msg_id,
    );
    tokio::spawn(proxy.serve());
    let aes_key_opt = if encrypt_message { Some(aes_key) } else { None };
    return Ok(Client::new(client, aes_key_opt));
}

async fn start_common_proxy_service(
    bind_info: phantom_network_adapter::tokio::BindInfo,
    aes_key: [u8; 32],
    encrypt_message: bool,
) -> Result<Client, ByteString> {
    let server_proxy_handshake = ServerProxyHandshake { aes_key: aes_key };
    let (sender, receiver) = TokioServerAdapter::build_channel(bind_info, server_proxy_handshake)
        .await
        .map_err(|err| -> ByteString {
            log::error!("{:?}", err);
            ByteString::from_static("启动代理服务失败")
        })?;
    let (proxy, client) = Proxy::new(
        (
            move |channel: Channel, proxy_msg: ProxyMsg<<MsgEncoder as Encoder>::MsgId>| {
                let sender = sender.clone();
                Box::pin(async move {
                    let encoder = MsgEncoder::new();
                    let raw_msg = encoder.encode_proxy_msg(proxy_msg);
                    sender.send_msg(&channel, &raw_msg).await
                })
            },
            receiver.filter_map(|(channel, msg)| {
                let ret = match msg {
                    phantom_network_adapter::NetEvent::Connected(service_version) => {
                        Some((channel, msg::NetEvent::Connected(service_version)))
                    }
                    phantom_network_adapter::NetEvent::Message(raw_msg) => {
                        let encoder = MsgEncoder::new();
                        match encoder.decode_worker_msg(raw_msg) {
                            Ok(worker_msg) => Some((channel, msg::NetEvent::Message(worker_msg))),
                            Err(err) => {
                                log::error!("收到异常数据包: {:?}", err);
                                None
                            }
                        }
                    }
                    phantom_network_adapter::NetEvent::Disconnected => {
                        Some((channel, msg::NetEvent::Disconnected))
                    }
                };
                return Box::pin(async { ret });
            }),
        ),
        new_msg_id,
    );
    tokio::spawn(proxy.serve());
    let aes_key_opt = if encrypt_message { Some(aes_key) } else { None };
    return Ok(Client::new(client, aes_key_opt));
}

pub async fn start_proxy_service(
    bind_info: BindInfo,
    aes_key: [u8; 32],
    encrypt_message: bool,
) -> Result<Client, ByteString> {
    match bind_info {
        BindInfo::Common(bind_info) => {
            let client = start_common_proxy_service(bind_info, aes_key, encrypt_message).await?;
            Ok(client)
        }
        BindInfo::Tls(bind_info) => {
            let client = start_tls_proxy_service(bind_info, aes_key, encrypt_message).await?;
            Ok(client)
        }
    }
}
