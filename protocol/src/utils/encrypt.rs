use crypto::aes::ecb_decryptor;
use crypto::aes::ecb_encryptor;
use crypto::aes::KeySize;
use crypto::blockmodes::PkcsPadding;
use crypto::buffer::{BufferResult, ReadBuffer, RefReadBuffer, RefWriteBuffer, WriteBuffer};
use crypto::symmetriccipher::SymmetricCipherError;
use rand::random;

pub fn fill_random_bytes(data: &mut [u8]) {
    for ch in data.iter_mut() {
        *ch = random();
    }
}

pub fn encrypt_by_aes_256(data: &[u8], key: &[u8; 32]) -> Result<Vec<u8>, SymmetricCipherError> {
    let mut encryptor = ecb_encryptor(KeySize::KeySize256, key, PkcsPadding);
    let mut final_result = Vec::<u8>::new();
    let mut read_buffer = RefReadBuffer::new(data);
    let mut buffer = [0; 4096];
    let mut write_buffer = RefWriteBuffer::new(&mut buffer);
    loop {
        let result = encryptor.encrypt(&mut read_buffer, &mut write_buffer, true)?;
        final_result.extend(
            write_buffer
                .take_read_buffer()
                .take_remaining()
                .iter()
                .map(|&i| i),
        );
        match result {
            BufferResult::BufferUnderflow => break,
            BufferResult::BufferOverflow => {}
        }
    }
    Ok(final_result)
}

pub fn decrypt_by_aes_256(data: &[u8], key: &[u8; 32]) -> Result<Vec<u8>, SymmetricCipherError> {
    let mut decryptor = ecb_decryptor(KeySize::KeySize256, key, PkcsPadding);
    let mut final_result = Vec::<u8>::new();
    let mut read_buffer = RefReadBuffer::new(data);
    let mut buffer = [0; 4096];
    let mut write_buffer = RefWriteBuffer::new(&mut buffer);
    loop {
        let result = decryptor.decrypt(&mut read_buffer, &mut write_buffer, true)?;
        final_result.extend(
            write_buffer
                .take_read_buffer()
                .take_remaining()
                .iter()
                .map(|&i| i),
        );
        match result {
            BufferResult::BufferUnderflow => break,
            BufferResult::BufferOverflow => {}
        }
    }
    Ok(final_result)
}
