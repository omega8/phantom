use super::worker::build_worker_future;
use super::BindInfo;
use super::Request;
use super::Response;
use crate::utils::encrypt;
use async_trait::async_trait;
use bytestring::ByteString;
use futures::stream::Stream;
use phantom_network_adapter::tokio::TokioServerAdapter;
use phantom_network_adapter::tokio_rustls::TokioRustlsServerAdapter;
use phantom_network_adapter::Handshake;
use phantom_network_adapter::MsgRead;
use phantom_network_adapter::MsgWrite;
use phantom_network_adapter::ServerAdapter;
use std::future::Future;

/**
 * 服务端Worker握手逻辑
 */
pub struct ServerWorkerHandshake {
    pub aes_key: [u8; 32],
    pub service_version: Option<ByteString>,
}

#[async_trait]
impl Handshake for ServerWorkerHandshake {
    type Output = Option<ByteString>;
    type Error = phantom_network_adapter::Error;
    async fn handshake(
        &self,
        reader: &mut (dyn MsgRead<Error = std::io::Error> + Send),
        writer: &mut (dyn MsgWrite<Error = std::io::Error> + Send),
    ) -> Result<Self::Output, Self::Error> {
        let mut random_data: [u8; 512] = [0; 512];
        encrypt::fill_random_bytes(&mut random_data);
        let mut plain = Vec::new();
        if let Some(service_version) = self.service_version.as_ref() {
            plain.extend(service_version.as_bytes());
        }
        plain.extend(random_data);
        let cipher = encrypt::encrypt_by_aes_256(&plain, &self.aes_key).map_err(|err| {
            log::error!("Aes encrypt random data of handshake failed: {:?}", err);
            phantom_network_adapter::Error::HandshakeError(ByteString::from_static(
                "Aes encrypt random data of handshake failed",
            ))
        })?;
        writer
            .write_msg(&cipher)
            .await
            .map_err(phantom_network_adapter::Error::IoError)?;
        let first_msg = reader
            .read_msg()
            .await
            .map_err(phantom_network_adapter::Error::IoError)?;
        if let Some(first_msg) = first_msg {
            if first_msg == &random_data[..] {
                return Ok(None);
            } else {
                return Err(phantom_network_adapter::Error::HandshakeError(
                    ByteString::from_static("Plain data from client is invalid"),
                ));
            }
        } else {
            return Err(phantom_network_adapter::Error::HandshakeError(
                ByteString::from_static("First message is none"),
            ));
        }
    }
}

pub async fn start_worker_service<Handler, HandlerResult, AbilityStream>(
    bind_info: BindInfo,
    handler: Handler,
    aes_key: [u8; 32],
    service_version: Option<ByteString>,
    encrypt_message: bool,
    ability_stream: AbilityStream,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>>
where
    Handler: Fn(Request) -> HandlerResult + Send + Sync + 'static,
    HandlerResult: Future<Output = Result<Response, ByteString>> + Send,
    AbilityStream: Stream<Item = u8> + Send + Unpin + 'static,
{
    let server_worker_handshake = ServerWorkerHandshake {
        aes_key: aes_key,
        service_version: service_version,
    };
    match bind_info {
        BindInfo::Common(bind_info) => {
            let bind_addr = bind_info.bind_addr;
            let (msg_sender, msg_receiver) =
                TokioServerAdapter::build_channel(bind_info, server_worker_handshake).await?;
            let aes_key_opt = if encrypt_message { Some(aes_key) } else { None };
            let service = build_worker_future(
                handler,
                msg_sender,
                msg_receiver,
                aes_key_opt,
                ability_stream,
            );
            log::info!("Listening on tcp://{}", bind_addr);
            println!("Listening on tcp://{}", bind_addr);
            return service.await;
        }
        BindInfo::Tls(bind_info) => {
            let bind_addr = bind_info.bind_addr;
            let (msg_sender, msg_receiver) =
                TokioRustlsServerAdapter::build_channel(bind_info, server_worker_handshake).await?;
            let aes_key_opt = if encrypt_message { Some(aes_key) } else { None };
            let service = build_worker_future(
                handler,
                msg_sender,
                msg_receiver,
                aes_key_opt,
                ability_stream,
            );
            log::info!("Listening on tcp://{}", bind_addr);
            println!("Listening on tcp://{}", bind_addr);
            return service.await;
        }
    };
}
