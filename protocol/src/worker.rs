use super::encoder::decode_request;
use super::encoder::encode_response;
use super::Request;
use super::Response;
use bytes::Bytes;
use bytestring::ByteString;
use futures::stream::Stream;
use futures::stream::StreamExt;
use phantom_core::encoder::Encoder;
use phantom_core::encoder::MsgEncoder;
use phantom_core::msg;
use phantom_core::msg::WorkerMsg;
use phantom_core::worker::Worker;
pub use phantom_network_adapter::tokio::ConnectInfo;
pub use phantom_network_adapter::tokio_rustls::ConnectInfo as TlsConnectInfo;
use phantom_network_adapter::Channel;
pub use phantom_network_adapter::HeartbeatConfig;
use phantom_network_adapter::MsgSender;
use phantom_network_adapter::NetEvent;
use std::future::Future;
use std::sync::Arc;
use std::vec;

pub fn build_worker_future<Handler, HandlerResult>(
    handler: Handler,
    msg_sender: impl MsgSender<Channel = Channel> + Send + Sync + Clone + 'static,
    msg_receiver: impl Stream<Item = (Channel, NetEvent)> + Send + Unpin + 'static,
    aes_key: Option<[u8; 32]>,
    ability_stream: impl Stream<Item = u8> + Send + Unpin + 'static,
) -> impl Future<Output = Result<(), Box<dyn std::error::Error + Send + Sync>>> + Send + 'static
where
    Handler: Fn(Request) -> HandlerResult + Send + Sync + 'static,
    HandlerResult: Future<Output = Result<Response, ByteString>> + Send,
{
    let handler = Arc::new(handler);
    let worker = Worker::new(
        (
            move |endpoint: Channel, worker_msg: WorkerMsg<<MsgEncoder as Encoder>::MsgId>| {
                let msg_sender = msg_sender.clone();
                Box::pin(async move {
                    let encoder = MsgEncoder::new();
                    let raw_msg = encoder.encode_worker_msg(worker_msg);
                    msg_sender.send_msg(&endpoint, &raw_msg).await
                })
            },
            msg_receiver.filter_map(move |(channel, event)| {
                let ret = match event {
                    phantom_network_adapter::NetEvent::Connected(service_version) => {
                        Some((channel, msg::NetEvent::Connected(service_version)))
                    }
                    phantom_network_adapter::NetEvent::Message(raw_msg) => {
                        let encoder = MsgEncoder::new();
                        match encoder.decode_proxy_msg(raw_msg) {
                            Ok(proxy_msg) => Some((channel, msg::NetEvent::Message(proxy_msg))),
                            Err(err) => {
                                log::error!("收到异常数据包: {:?}", err);
                                None
                            }
                        }
                    }
                    phantom_network_adapter::NetEvent::Disconnected => {
                        Some((channel, msg::NetEvent::Disconnected))
                    }
                };
                return Box::pin(async { ret });
            }),
        ),
        move |req: Bytes| {
            let req_ret = decode_request(req, aes_key);
            let handler = handler.clone();
            async move {
                match req_ret {
                    Ok(req) => {
                        let resp_ret = handler(req).await;
                        match resp_ret {
                            Ok(resp) => match encode_response(&resp, aes_key, Some(vec![0])) {
                                Ok(resp) => Bytes::from(resp),
                                Err(err) => {
                                    let err_msg = format!("Bad response! {}", err);
                                    let mut resp = Vec::with_capacity(1 + err_msg.len());
                                    resp.push(1); //1表示失败，0表示成功
                                    resp.append(&mut err_msg.into_bytes());
                                    Bytes::from(resp)
                                }
                            },
                            Err(err_msg) => {
                                let mut resp = Vec::with_capacity(1 + err_msg.len());
                                resp.push(1); //1表示失败，0表示成功
                                resp.extend_from_slice(&err_msg.as_bytes());
                                Bytes::from(resp)
                            }
                        }
                    }
                    Err(err) => {
                        let err_msg = format!("Bad request! {}", err);
                        let mut resp = Vec::with_capacity(1 + err_msg.len());
                        resp.push(1); //1表示失败，0表示成功
                        resp.extend_from_slice(&err_msg.as_bytes());
                        Bytes::from(resp)
                    }
                }
            }
        },
        |task| {
            tokio::spawn(task);
        },
        ability_stream,
    );
    return worker.serve();
}
