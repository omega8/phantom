use super::encoder::decode_response;
use super::encoder::encode_request;
use super::Request;
use super::Response;
use bytes::Bytes;
use bytestring::ByteString;
use phantom_core::proxy::ProxyClient;
use phantom_network_adapter::Channel;

pub fn read_u8(mut input: Bytes) -> Result<(u8, Bytes), &'static str> {
    let min_len = 1;
    if input.len() < min_len {
        return Err("输入数据长度不够");
    } else {
        let rest = input.split_off(min_len);
        return Ok((input[0], rest));
    }
}

#[derive(Clone)]
pub struct Client {
    proxy_client: ProxyClient<Channel, u128>,
    aes_key: Option<[u8; 32]>,
}

impl Client {
    pub fn new(proxy_client: ProxyClient<Channel, u128>, aes_key: Option<[u8; 32]>) -> Self {
        Client {
            proxy_client: proxy_client,
            aes_key: aes_key,
        }
    }
    pub async fn call_remote(
        &self,
        request: &Request,
        version: Option<ByteString>,
    ) -> Result<Response, Box<dyn std::error::Error + Send + Sync>> {
        let req_msg = encode_request(&request, self.aes_key, None)?;
        let resp = self
            .proxy_client
            .call_remote(req_msg.into(), version)
            .await
            .map_err(|err| -> Box<dyn std::error::Error + Send + Sync> {
                match err {
                    phantom_core::proxy::Error::NoWorker => "no worker".into(),
                    phantom_core::proxy::Error::WorkerDisconnected(channel_id) => {
                        format!("worker disconnected {:?}", channel_id).into()
                    }
                    _ => "Internal Server Error".into(),
                }
            })?;
        let (err_code, rest) = read_u8(resp)?;
        if 0 == err_code {
            return decode_response(rest, self.aes_key);
        } else {
            return Err(std::str::from_utf8(&rest)?.to_string().into());
        }
    }
}
