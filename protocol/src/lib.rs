pub mod client_proxy;
pub mod client_worker;
pub mod encoder;
pub mod proxy;
pub mod server_proxy;
pub mod server_worker;
pub mod utils;
mod worker;
use bytes::Bytes;
use bytestring::ByteString;
use std::collections::HashMap;
use uuid::Uuid;

pub struct Request {
    pub uri: ByteString,
    pub ip: ByteString,
    pub session: Option<ByteString>,
    pub versions: Option<HashMap<String, String>>,
    pub headers: Option<HashMap<String, Option<String>>>,
    pub body: Bytes,
}

pub struct Response {
    pub session: Option<ByteString>,
    pub body: Bytes,
}

fn new_msg_id() -> u128 {
    let token = Uuid::new_v4();
    token.as_u128()
}

#[derive(Debug, Clone)]
pub enum BindInfo {
    Common(phantom_network_adapter::tokio::BindInfo),
    Tls(phantom_network_adapter::tokio_rustls::BindInfo),
}

#[derive(Debug, Clone)]
pub enum ConnectParams {
    Common(phantom_network_adapter::tokio::ConnectInfo),
    Tls(phantom_network_adapter::tokio_rustls::ConnectInfo),
}
