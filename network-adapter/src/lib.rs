pub mod protocol;
pub mod tokio;
pub mod tokio_rustls;

use ::tokio::io::{AsyncReadExt, AsyncWriteExt};
use async_trait::async_trait;
use bytes::Bytes;
use bytestring::ByteString;
use futures::channel::mpsc::UnboundedSender;
use futures::prelude::sink::SinkExt;
use futures::stream::Stream;
use log;
use protocol::Decoder;
use protocol::FramedLayer;
use protocol::FramedState;
use protocol::Layer;
use std::fmt;
use std::fmt::Debug;
use std::fmt::Display;
use std::hash::Hash;
use std::io::ErrorKind;
use std::time::Duration;
use thiserror::Error;

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Channel(pub u64);

impl fmt::Display for Channel {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(Clone)]
pub struct ServerMgr<T> {
    sender: UnboundedSender<(bool, T)>,
}

impl<T> ServerMgr<T> {
    fn new(sender: UnboundedSender<(bool, T)>) -> ServerMgr<T> {
        ServerMgr { sender: sender }
    }
    pub async fn add_server(&self, connect_info: T) {
        if let Err(err) = self.sender.clone().send((true, connect_info)).await {
            log::error!("{:?}", err);
        }
    }
    pub async fn remove_server(&self, connect_info: T) {
        if let Err(err) = self.sender.clone().send((false, connect_info)).await {
            log::error!("{:?}", err);
        }
    }
}

pub const HEARTBEAT_INTERVAL: u64 = 3;
pub const HEARTBEAT_TIMEOUT: u64 = 15;

#[derive(Debug, Clone)]
pub struct HeartbeatConfig {
    pub interval: Duration,
    pub timeout: Duration,
}

impl Default for HeartbeatConfig {
    fn default() -> Self {
        HeartbeatConfig {
            interval: Duration::from_secs(HEARTBEAT_INTERVAL),
            timeout: Duration::from_secs(HEARTBEAT_TIMEOUT),
        }
    }
}

#[async_trait]
pub trait MsgRead {
    type Error;
    async fn read_msg(&mut self) -> Result<Option<Bytes>, Self::Error>;
}

#[async_trait]
pub trait MsgWrite {
    type Error;
    async fn write_msg(&mut self, msg: &[u8]) -> Result<(), Self::Error>;
}

pub struct MsgReader<T> {
    lower_reader: T,
    framed_state: FramedState,
    timeout: Duration,
}

impl<T> MsgReader<T> {
    pub fn new(lower_reader: T, timeout: Duration) -> Self {
        MsgReader {
            lower_reader: lower_reader,
            framed_state: FramedState::new(),
            timeout: timeout,
        }
    }
}

const BUF_LEN: usize = 1024;

#[async_trait]
impl<T> MsgRead for MsgReader<T>
where
    T: AsyncReadExt + Unpin + Send,
{
    type Error = std::io::Error;
    async fn read_msg(&mut self) -> Result<Option<Bytes>, Self::Error> {
        let mut buffer = [0; BUF_LEN];
        loop {
            if let Some(msg) = self.framed_state.next() {
                return Ok(Some(msg));
            }
            ::tokio::select! {
                count_ret = self.lower_reader.read(&mut buffer) => {
                    match count_ret {
                        Ok(count) => {
                            if 0 < count {
                                let chunk = &buffer[..count];
                                self.framed_state.append(chunk);
                            } else {
                                break;
                            }
                        }
                        Err(err) => {
                            let error_kind = err.kind();
                            if ErrorKind::Interrupted == error_kind || ErrorKind::WouldBlock == error_kind {
                                continue;
                            } else {
                                if ErrorKind::ConnectionReset == error_kind {
                                    log::info!("connection reset.");
                                    break;
                                } else {
                                    log::error!("TCP receive error: {}", err);
                                }
                                return Err(err);
                            }
                        }
                    }
                },
                _ = ::tokio::time::sleep(self.timeout) => {
                    log::error!("Read message timeout");
                    return Err(std::io::Error::new(ErrorKind::TimedOut, "Read message timeout"));
                }
            };
        }
        return Ok(None);
    }
}

#[async_trait]
impl<T> MsgWrite for T
where
    T: AsyncWriteExt + Unpin + Send,
{
    type Error = std::io::Error;
    async fn write_msg(&mut self, msg: &[u8]) -> Result<(), Self::Error> {
        let mut buffer = Vec::new();
        FramedLayer::encode(&mut buffer, msg);
        self.write_all(&buffer).await?;
        return Ok(());
    }
}

#[derive(Debug)]
pub enum NetEvent {
    Message(Bytes),
    Connected(Option<ByteString>),
    Disconnected,
}

#[async_trait]
pub trait MsgSender: Clone {
    type Channel;
    async fn send_msg(
        &self,
        channel: &Self::Channel,
        msg: &[u8],
    ) -> Result<(), Box<dyn std::error::Error + Send + Sync>>;
}

#[async_trait]
pub trait ClientAdapter {
    type Channel: Clone + Eq + Hash + Display + Debug;
    type ConnectInfo;
    type MsgSender: MsgSender<Channel = Self::Channel>;
    type MsgReceiver: Stream<Item = (Self::Channel, NetEvent)> + Send + Unpin;
    type Handshake: Handshake<Error = Self::Error> + 'static;
    type Error;
    async fn build_channel(
        handshake: Self::Handshake,
    ) -> (
        Self::MsgSender,
        Self::MsgReceiver,
        ServerMgr<Self::ConnectInfo>,
    );
}

#[async_trait]
pub trait Handshake: Send + Sync {
    type Error;
    type Output;
    async fn handshake(
        &self,
        reader: &mut (dyn MsgRead<Error = std::io::Error> + Send),
        writer: &mut (dyn MsgWrite<Error = std::io::Error> + Send),
    ) -> Result<Self::Output, Self::Error>;
}

#[async_trait]
pub trait ServerAdapter {
    type Channel: Clone + Eq + Hash + Display + Debug;
    type BindInfo;
    type MsgSender: MsgSender<Channel = Self::Channel>;
    type MsgReceiver: Stream<Item = (Self::Channel, NetEvent)> + Send + Unpin;
    type Handshake: Handshake<Error = Self::Error> + 'static;
    type Error;
    async fn build_channel(
        bind_info: Self::BindInfo,
        handshake: Self::Handshake,
    ) -> Result<(Self::MsgSender, Self::MsgReceiver), Box<dyn std::error::Error + Send + Sync>>;
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("Io error")]
    IoError(std::io::Error),
    #[error("Handshake failed, `{0}`")]
    HandshakeError(ByteString),
}
