use super::protocol::FramedLayer;
use super::protocol::Layer;
use super::Channel;
use super::ClientAdapter;
use super::Handshake;
use super::HeartbeatConfig;
use super::MsgRead;
use super::MsgReader;
use super::MsgSender;
use super::NetEvent;
use super::ServerAdapter;
use super::ServerMgr;
use async_trait::async_trait;
use bytestring::ByteString;
use futures::lock::Mutex;
use futures::stream::Stream;
use futures::stream::StreamExt;
use log;
use std::io;
use std::marker::PhantomData;
use std::net::SocketAddr;
use std::pin::Pin;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::task::{Context, Poll};
use std::time::Duration;
use tokio;
use tokio::io::ReadHalf;
use tokio::io::WriteHalf;
use tokio::io::{split, AsyncWriteExt};
use tokio::net::TcpListener;
use tokio::net::TcpSocket;
use tokio::net::TcpStream;
use tokio::sync::mpsc::UnboundedSender;
use tokio::sync::RwLock;
use tokio_stream::wrappers::UnboundedReceiverStream;

#[derive(Debug, Clone)]
pub struct ConnectInfo {
    pub server_addr: SocketAddr,
    pub heartbeat_config: HeartbeatConfig,
}

impl PartialEq for ConnectInfo {
    fn eq(&self, other: &ConnectInfo) -> bool {
        return self.server_addr == other.server_addr;
    }
}

pub struct TokioClientAdapter<H> {
    phantom1: PhantomData<H>,
}

#[derive(Clone)]
pub struct ClientMsgSender {
    servers: Arc<
        RwLock<
            Vec<(
                Arc<ConnectInfo>,
                Option<(
                    Arc<Mutex<WriteHalf<TcpStream>>>,
                    Channel,
                    Option<ByteString>,
                )>,
            )>,
        >,
    >,
}

#[async_trait]
impl MsgSender for ClientMsgSender {
    type Channel = Channel;
    async fn send_msg(
        &self,
        server: &Channel,
        msg: &[u8],
    ) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
        let mut writer: Option<Arc<Mutex<WriteHalf<TcpStream>>>> = None;
        for item in self.servers.read().await.iter() {
            if let Some(item) = item.1.as_ref() {
                if &item.1 == server {
                    writer.replace(item.0.clone());
                    break;
                }
            }
        }
        if let Some(writer) = writer {
            let mut buffer = Vec::new();
            let mut biz_msg: Vec<u8> = Vec::with_capacity(msg.len() + 1);
            biz_msg.push(0);
            biz_msg.extend_from_slice(msg);
            FramedLayer::encode(&mut buffer, &biz_msg);
            writer.lock().await.write_all(&buffer).await?;
        }
        return Ok(());
    }
}

pub struct MsgReceiver {
    read_stream: UnboundedReceiverStream<(Channel, super::NetEvent)>,
}

impl Stream for MsgReceiver {
    type Item = (Channel, super::NetEvent);
    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        return Stream::poll_next(Pin::new(&mut self.read_stream), cx);
    }
}

async fn start_connect_lifecycle(
    connect_info: Arc<ConnectInfo>,
    handshake: Arc<impl Handshake<Error = super::Error, Output = Option<ByteString>> + 'static>,
    channel: Channel,
    servers: Arc<
        RwLock<
            Vec<(
                Arc<ConnectInfo>,
                Option<(
                    Arc<Mutex<WriteHalf<TcpStream>>>,
                    Channel,
                    Option<ByteString>,
                )>,
            )>,
        >,
    >,
    sender: UnboundedSender<(Channel, NetEvent)>,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let server_addr = connect_info.server_addr;
    let (reader, mut writer) = connect_server(server_addr).await.map_err(|err| {
        log::error!("Connect to server {} failed.", server_addr);
        err
    })?;
    log::info!("Connected to server {}. Begin handshake.", server_addr);
    let mut reader = MsgReader::new(reader, connect_info.heartbeat_config.timeout);
    let service_version = handshake
        .handshake(&mut reader, &mut writer)
        .await
        .map_err(|err| {
            log::error!("Error occured while shaking hands with server: {}.", err);
            err
        })?;
    log::info!("Shake hands with server {} succeed.", server_addr);
    let writer = Arc::new(Mutex::new(writer));
    for item in servers.write().await.iter_mut() {
        if item.0 == connect_info {
            item.1 = Some((writer.clone(), channel, service_version.clone()));
            break;
        }
    }
    if let Err(err) = sender.send((channel, NetEvent::Connected(service_version))) {
        log::error!("Send message NetEvent::Connected failed {:?}", err);
    }
    let is_end = Arc::new(AtomicBool::new(false));
    let is_end_clone = is_end.clone();
    let interval = connect_info.heartbeat_config.interval;
    tokio::spawn(async move {
        let writer = writer.clone();
        loop {
            if is_end_clone.load(Ordering::Relaxed) {
                break;
            }
            tokio::time::sleep(interval).await;
            if is_end_clone.load(Ordering::Relaxed) {
                break;
            }
            let mut buffer = Vec::new();
            FramedLayer::encode(&mut buffer, &[1]);
            if let Err(err) = writer.lock().await.write_all(&buffer).await {
                log::error!("Send heartbeat message failed {:?}", err);
            }
        }
    });
    loop {
        match reader.read_msg().await {
            Ok(msg_opt) => {
                if let Some(mut msg) = msg_opt {
                    if msg.is_empty() {
                        //异常数据格式
                    } else {
                        let rest = msg.split_off(1);
                        match msg.first() {
                            Some(0) => {
                                //业务消息
                                if let Err(err) = sender.send((channel, NetEvent::Message(rest))) {
                                    log::error!("Send message NetEvent::Message failed {:?}", err);
                                }
                            }
                            Some(1) => {
                                //keepalive消息
                            }
                            _ => {
                                //其他异常消息
                            }
                        }
                    }
                } else {
                    break;
                }
            }
            Err(err) => {
                log::error!("Read message error: {}", err);
                break;
            }
        }
    }
    is_end.store(true, Ordering::Relaxed);
    log::info!("Disconnected: {}.", server_addr);
    let mut servers = servers.write().await;
    for item in servers.iter_mut() {
        if item.0 == connect_info {
            item.1 = None;
            break;
        }
    }
    drop(servers);
    if let Err(err) = sender.send((channel, NetEvent::Disconnected)) {
        log::error!("Send message NetEvent::Disconnected failed {:?}", err);
    }
    return Ok(());
}

#[async_trait]
impl<H> ClientAdapter for TokioClientAdapter<H>
where
    H: Handshake<Error = super::Error, Output = Option<ByteString>> + 'static,
{
    type Channel = Channel;
    type ConnectInfo = ConnectInfo;
    type MsgSender = ClientMsgSender;
    type MsgReceiver = MsgReceiver;
    type Handshake = H;
    type Error = super::Error;
    async fn build_channel(handshake: H) -> (ClientMsgSender, MsgReceiver, ServerMgr<ConnectInfo>) {
        let (sender, mut change_stream) =
            futures::channel::mpsc::unbounded::<(bool, ConnectInfo)>();
        let server_mgr = ServerMgr::new(sender);
        let servers: Arc<
            RwLock<
                Vec<(
                    Arc<ConnectInfo>,
                    Option<(
                        Arc<Mutex<WriteHalf<TcpStream>>>,
                        Channel,
                        Option<ByteString>,
                    )>,
                )>,
            >,
        > = Arc::new(RwLock::new(Vec::new()));
        let servers2 = servers.clone();
        let (sender, receiver) =
            tokio::sync::mpsc::unbounded_channel::<(Channel, super::NetEvent)>();
        let read_stream = UnboundedReceiverStream::new(receiver);
        tokio::spawn(async move {
            let mut channel_id = 0;
            let handshake = Arc::new(handshake);
            while let Some((add, connect_info)) = change_stream.next().await {
                let connect_info = Arc::new(connect_info);
                if add {
                    if servers
                        .read()
                        .await
                        .iter()
                        .all(|item| item.0 != connect_info)
                    {
                        servers.write().await.push((connect_info.clone(), None));
                        let sender = sender.clone();
                        let servers = servers.clone();
                        let connect_info = connect_info.clone();
                        channel_id += 1;
                        let channel = Channel(channel_id);
                        let handshake = handshake.clone();
                        tokio::spawn(async move {
                            loop {
                                if servers
                                    .read()
                                    .await
                                    .iter()
                                    .any(|item| item.0 == connect_info && item.1.is_none())
                                {
                                    if let Err(err) = start_connect_lifecycle(
                                        connect_info.clone(),
                                        handshake.clone(),
                                        channel,
                                        servers.clone(),
                                        sender.clone(),
                                    )
                                    .await
                                    {
                                        log::error!("{:?}", err);
                                    }
                                    log::info!(
                                        "Try to connect to {} after 1s.",
                                        connect_info.server_addr
                                    );
                                    tokio::time::sleep(Duration::new(1, 0)).await;
                                } else {
                                    break;
                                }
                            }
                        });
                    }
                } else {
                    servers
                        .write()
                        .await
                        .retain(|server| server.0 != connect_info);
                }
            }
        });
        let msg_sender = ClientMsgSender {
            servers: servers2.clone(),
        };
        let msg_receiver = MsgReceiver {
            read_stream: read_stream,
        };
        return (msg_sender, msg_receiver, server_mgr);
    }
}

#[derive(Debug, Clone)]
pub struct BindInfo {
    pub bind_addr: SocketAddr,
    pub heartbeat_config: HeartbeatConfig,
}

#[derive(Clone)]
pub struct ServerMsgSender {
    clients: Arc<
        RwLock<
            Vec<(
                Arc<Mutex<WriteHalf<TcpStream>>>,
                Channel,
                Option<ByteString>,
            )>,
        >,
    >,
}

// impl ServerMsgSender {
//     pub async fn find_clients(&self, filter: impl Fn(&Option<CowStr>) -> bool) -> Vec<Channel> {
//         let clients = self.clients.read().await;
//         clients
//             .iter()
//             .filter_map(|item| {
//                 if filter(&item.2) {
//                     Some(item.1.clone())
//                 } else {
//                     None
//                 }
//             })
//             .collect()
//     }
// }

#[async_trait]
impl MsgSender for ServerMsgSender {
    type Channel = Channel;
    async fn send_msg(
        &self,
        client: &Channel,
        msg: &[u8],
    ) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
        let mut writer: Option<Arc<Mutex<WriteHalf<TcpStream>>>> = None;
        for item in self.clients.read().await.iter() {
            if &item.1 == client {
                writer.replace(item.0.clone());
                break;
            }
        }
        if let Some(writer) = writer {
            let mut buffer = Vec::new();
            let mut biz_msg: Vec<u8> = Vec::with_capacity(msg.len() + 1);
            biz_msg.push(0);
            biz_msg.extend_from_slice(msg);
            FramedLayer::encode(&mut buffer, &biz_msg);
            writer.lock().await.write_all(&buffer).await?;
        }
        return Ok(());
    }
}

async fn start_accept_lifecycle(
    peer_addr: SocketAddr,
    stream: TcpStream,
    handshake: Arc<impl Handshake<Error = super::Error, Output = Option<ByteString>> + 'static>,
    channel: Channel,
    clients: Arc<
        RwLock<
            Vec<(
                Arc<Mutex<WriteHalf<TcpStream>>>,
                Channel,
                Option<ByteString>,
            )>,
        >,
    >,
    sender: UnboundedSender<(Channel, NetEvent)>,
    heartbeat_config: HeartbeatConfig,
) -> Result<(), super::Error> {
    log::info!("Connected to client {}. Begin handshake.", peer_addr);
    let (reader, mut writer) = split(stream);
    let mut reader = MsgReader::new(reader, heartbeat_config.timeout);
    let service_version = handshake
        .handshake(&mut reader, &mut writer)
        .await
        .map_err(|err| {
            log::error!("Error occured while validating client connection: {}.", err);
            err
        })?;
    log::info!("Shake hands with client {} succeed.", peer_addr);
    let writer = Arc::new(Mutex::new(writer));
    clients
        .write()
        .await
        .push((writer.clone(), channel, service_version.clone()));
    if let Err(err) = sender.send((channel, NetEvent::Connected(service_version))) {
        log::error!("Send message NetEvent::Connected failed {:?}", err);
    }
    let is_end = Arc::new(AtomicBool::new(false));
    let is_end_clone = is_end.clone();
    tokio::spawn(async move {
        let writer = writer.clone();
        loop {
            if is_end_clone.load(Ordering::Relaxed) {
                break;
            }
            tokio::time::sleep(heartbeat_config.interval).await;
            if is_end_clone.load(Ordering::Relaxed) {
                break;
            }
            let mut buffer = Vec::new();
            FramedLayer::encode(&mut buffer, &[1]);
            if let Err(err) = writer.lock().await.write_all(&buffer).await {
                log::error!("Send heartbeat message failed {:?}", err);
            }
        }
    });
    loop {
        match reader.read_msg().await {
            Ok(msg_opt) => {
                if let Some(mut msg) = msg_opt {
                    if msg.is_empty() {
                        //异常数据格式
                    } else {
                        let rest = msg.split_off(1);
                        match msg.first() {
                            Some(0) => {
                                //业务消息
                                if let Err(err) = sender.send((channel, NetEvent::Message(rest))) {
                                    log::error!("Send message NetEvent::Message failed {:?}", err);
                                }
                            }
                            Some(1) => {
                                //keepalive消息
                            }
                            _ => {
                                //其他异常消息
                            }
                        }
                    }
                } else {
                    break;
                }
            }
            Err(err) => {
                log::error!("Read message error: {}", err);
                break;
            }
        }
    }
    is_end.store(true, Ordering::Relaxed);
    log::info!("Disconnected: {}.", peer_addr);
    let mut clients = clients.write().await;
    clients.retain(|item| item.1 != channel);
    drop(clients);
    if let Err(err) = sender.send((channel, NetEvent::Disconnected)) {
        log::error!("Send message NetEvent::Disconnected failed {:?}", err);
    }
    return Ok(());
}

pub struct TokioServerAdapter<H> {
    phantom1: PhantomData<H>,
}

#[async_trait]
impl<H> ServerAdapter for TokioServerAdapter<H>
where
    H: Handshake<Error = super::Error, Output = Option<ByteString>> + 'static,
{
    type Channel = Channel;
    type BindInfo = BindInfo;
    type MsgSender = ServerMsgSender;
    type MsgReceiver = MsgReceiver;
    type Handshake = H;
    type Error = super::Error;
    async fn build_channel(
        bind_info: BindInfo,
        handshake: H,
    ) -> Result<(ServerMsgSender, MsgReceiver), Box<dyn std::error::Error + Send + Sync>> {
        let listener = TcpListener::bind(bind_info.bind_addr).await?;
        let clients: Arc<
            RwLock<
                Vec<(
                    Arc<Mutex<WriteHalf<TcpStream>>>,
                    Channel,
                    Option<ByteString>,
                )>,
            >,
        > = Arc::new(RwLock::new(Vec::new()));
        let (sender, receiver) =
            tokio::sync::mpsc::unbounded_channel::<(Channel, super::NetEvent)>();
        let read_stream = tokio_stream::wrappers::UnboundedReceiverStream::new(receiver);
        let clients2 = clients.clone();
        let heartbeat_config = bind_info.heartbeat_config;
        tokio::spawn(async move {
            let mut channel_id = 0;
            let handshake = Arc::new(handshake);
            loop {
                match listener.accept().await {
                    Ok((stream, peer_addr)) => {
                        channel_id += 1;
                        let channel = Channel(channel_id);
                        let sender = sender.clone();
                        let clients = clients2.clone();
                        let handshake = handshake.clone();
                        let heartbeat_config = heartbeat_config.clone();
                        tokio::spawn(async move {
                            if let Err(err) = start_accept_lifecycle(
                                peer_addr,
                                stream,
                                handshake,
                                channel,
                                clients,
                                sender,
                                heartbeat_config,
                            )
                            .await
                            {
                                log::error!("{:?}", err);
                            }
                        });
                    }
                    Err(err) => {
                        log::error!("Tcp accept error: {:?}", err);
                    }
                }
            }
        });
        let msg_sender = ServerMsgSender {
            clients: clients.clone(),
        };
        let msg_receiver = MsgReceiver {
            read_stream: read_stream,
        };
        return Ok((msg_sender, msg_receiver));
    }
}

async fn connect_server(
    addr: SocketAddr,
) -> Result<(ReadHalf<TcpStream>, WriteHalf<TcpStream>), io::Error> {
    let socket = match addr {
        SocketAddr::V4(_) => TcpSocket::new_v4()?,
        SocketAddr::V6(_) => TcpSocket::new_v6()?,
    };
    let stream = socket.connect(addr).await?;
    let (reader, writer) = split(stream);
    return Ok((reader, writer));
}
