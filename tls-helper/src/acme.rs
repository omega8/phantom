use super::cache_mgr::CacheData;
use super::cache_mgr::CacheMgr;
use super::handler::Handler;
use async_acme::acme::ACME_TLS_ALPN_NAME;
use async_acme::acme::LETS_ENCRYPT_PRODUCTION_DIRECTORY;
use async_acme::rustls_helper::order;
use async_trait::async_trait;
use rustls::server::{ClientHello, ResolvesServerCert};
use rustls::sign::CertifiedKey;
use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;

pub struct AcmeRequestCert {
    pub cert_order_cache: Arc<tokio::sync::RwLock<HashMap<String, Arc<CertOrderCacheMgr>>>>,
    pub cert_resolver: Arc<ResolveServerCert>,
    pub account_dir: PathBuf,
    pub contact: Vec<String>,
    pub update_interval: u64,
}

#[async_trait]
impl Handler<Arc<str>> for AcmeRequestCert {
    type Out = Result<(), Box<dyn std::error::Error + Send + Sync>>;
    async fn handle(&self, server_name: Arc<str>) -> Self::Out {
        if let Some(certified_key) = self.cert_resolver.get_cert(&server_name) {
            if certified_key.expired() {
                self.request_cert(&server_name).await?;
            }
        } else {
            self.request_cert(&server_name).await?;
        }
        return Ok(());
    }
}

impl AcmeRequestCert {
    async fn request_cert(&self, server_name: &str) -> Result<CacheData<CertifiedKey>, String> {
        let cert_resolver = self.cert_resolver.clone();
        let cert_order_cache = self.cert_order_cache.read().await;
        let cert_order_cache_mgr = cert_order_cache.get(server_name).map(Clone::clone);
        drop(cert_order_cache);
        let cache_data = if let Some(cert_order_cache_mgr) = cert_order_cache_mgr {
            cert_order_cache_mgr.get_fresh_data_full().await?
        } else {
            let mut cert_order_cache = self.cert_order_cache.write().await;
            let cert_order_cache_mgr = cert_order_cache
                .entry(server_name.to_string())
                .or_insert_with(|| {
                    let account_dir = self.account_dir.clone();
                    let contact = self.contact.clone();
                    Arc::new(CacheMgr::new(
                        CertOrderHandler {
                            account_dir: account_dir,
                            contact: contact,
                            cert_resolver: cert_resolver,
                            update_interval: self.update_interval,
                        },
                        server_name.to_string(),
                    ))
                })
                .clone();
            drop(cert_order_cache);
            cert_order_cache_mgr.get_fresh_data_full().await?
        };
        self.cert_resolver
            .certs
            .write()
            .unwrap()
            .insert(server_name.to_string(), cache_data.clone());
        return Ok(cache_data);
    }
}

#[derive(Default)]
pub struct ResolveServerCert {
    pub certs: Arc<std::sync::RwLock<HashMap<String, CacheData<CertifiedKey>>>>,
    pub auth_keys: Arc<std::sync::RwLock<HashMap<String, Arc<CertifiedKey>>>>,
}

impl std::fmt::Debug for ResolveServerCert {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ResolveServerCert").finish()
    }
}

impl ResolvesServerCert for ResolveServerCert {
    fn resolve(&self, client_hello: ClientHello) -> Option<Arc<CertifiedKey>> {
        let server_name = client_hello.server_name()?;
        if is_tls_alpn_challenge(&client_hello) {
            self.get_auth_key(server_name)
        } else {
            self.get_cert(server_name)
                .map(|cache_data| Arc::new(cache_data.data.clone()))
        }
    }
}

impl ResolveServerCert {
    pub fn new() -> ResolveServerCert {
        Self::default()
    }
    pub fn get_cert(&self, server_name: &str) -> Option<CacheData<CertifiedKey>> {
        self.certs.read().unwrap().get(server_name).cloned()
    }
    fn get_auth_key(&self, server_name: &str) -> Option<Arc<CertifiedKey>> {
        self.auth_keys.read().unwrap().get(server_name).cloned()
    }
    // pub fn remove_cert_if_expired(&self, server_name: &str) {
    //     let mut certs = self.certs.write().unwrap();
    //     if let Some(cert) = certs.get(server_name) {
    //         if cert.expired() {
    //             certs.remove(server_name);
    //         }
    //     }
    // }
}

pub fn is_tls_alpn_challenge(client_hello: &ClientHello) -> bool {
    return client_hello
        .alpn()
        .and_then(|mut iter| iter.find(|alpn| *alpn == ACME_TLS_ALPN_NAME))
        .is_some();
}

pub struct CertOrderHandler {
    pub account_dir: PathBuf,
    pub contact: Vec<String>,
    pub cert_resolver: Arc<ResolveServerCert>,
    pub update_interval: u64,
}

#[async_trait]
impl Handler<String> for CertOrderHandler {
    type Out = Result<(CertifiedKey, Duration), String>;
    async fn handle(&self, server_name: String) -> Self::Out {
        let certified_key = order(
            |key, cert| {
                self.cert_resolver
                    .auth_keys
                    .write()
                    .unwrap()
                    .insert(key, Arc::new(cert));
                Ok(())
            },
            LETS_ENCRYPT_PRODUCTION_DIRECTORY,
            &[server_name.clone()],
            Some(&self.account_dir),
            &self.contact,
        )
        .await
        .map_err(|err| {
            log::error!("Apply certificate for domain \"{}\" failed: {:?}", server_name, err);
            err.to_string()
        })?;
        return Ok((certified_key, Duration::from_secs(self.update_interval)));
    }
}

pub type CertOrderCacheMgr = CacheMgr<CertOrderHandler, String, CertifiedKey, String>;
