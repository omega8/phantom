use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Response<T> {
    pub code: i32,
    pub data: Option<T>,
    pub message: String,
}

impl<T> Response<T> {
    pub fn success(data: Option<T>) -> Response<T> {
        return Response {
            code: 0,
            data: data,
            message: String::from("success"),
        };
    }
    pub fn failure(mut code: i32, msg: String, data: Option<T>) -> Response<T> {
        if 0 == code {
            code = -1;
        }
        return Response {
            code: code,
            data: data,
            message: msg,
        };
    }
}

pub fn get_resp_data<T>(resp: Response<T>) -> Result<T, String> {
    if 0 == resp.code {
        return resp.data.ok_or_else(|| {
            return String::from("没有返回数据！");
        });
    } else {
        return Err(resp.message);
    }
}
