use crate::acme::ResolveServerCert;
use crate::cache_mgr::CacheMgr;
use crate::handler::Handler;
pub use async_acme;
use rustls::crypto::ring::sign::any_supported_type;
use rustls::{server::ResolvesServerCert, sign::CertifiedKey};
use rustls_pemfile::certs;
use rustls_pemfile::private_key;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::path::Path;
use std::path::PathBuf;
use std::sync::Arc;
use tokio::sync::RwLock;

pub mod acme;
pub mod api_server;
pub mod cache_mgr;
pub mod common;
pub mod handler;
pub mod multiple;
pub mod single;

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
pub enum CertConfig {
    Single {
        server_cert: String,
        server_key: String,
        update_interval: u64,
    },
    Multiple {
        cert_dir: String,
        update_interval: u64,
    },
    ApiServer {
        api_url: String,
        api_key: Option<String>,
    },
    Acme {
        account_dir: PathBuf,
        contact: Vec<String>,
        update_interval: u64,
    },
}

pub fn build_cert_resolver(
    cert_config: CertConfig,
) -> Result<
    (
        Arc<dyn ResolvesServerCert>,
        Arc<dyn Handler<Arc<str>, Out = Result<(), Box<dyn std::error::Error + Send + Sync>>>>,
    ),
    Box<dyn std::error::Error + Send + Sync>,
> {
    match cert_config {
        CertConfig::Single {
            server_cert,
            server_key,
            update_interval,
        } => {
            let cert = Arc::new(single::Cert::new());
            return Ok((
                cert.clone(),
                Arc::new(single::SingleRequestCert {
                    cert_cache_mgr: CacheMgr::new(
                        single::SingleCertHandler {
                            server_cert: server_cert,
                            server_key: server_key,
                            update_interval: update_interval,
                        },
                        (),
                    ),
                    cert: cert,
                }),
            ));
        }
        CertConfig::Multiple {
            cert_dir,
            update_interval,
        } => {
            let certs = Arc::new(multiple::Certs::new());
            return Ok((
                certs.clone(),
                Arc::new(multiple::MultipleRequestCert {
                    cert_cache: RwLock::new(HashMap::new()),
                    certs: certs,
                    handler: multiple::MultipleCertHandler {
                        cert_dir: cert_dir,
                        update_interval: update_interval,
                    },
                }),
            ));
        }
        CertConfig::ApiServer { api_url, api_key } => {
            let certs = Arc::new(multiple::Certs::new());
            return Ok((
                certs.clone(),
                Arc::new(multiple::MultipleRequestCert {
                    cert_cache: RwLock::new(HashMap::new()),
                    certs: certs,
                    handler: api_server::ApiServerCertHandler {
                        api_url: api_url,
                        api_key: api_key,
                    },
                }),
            ));
        }
        CertConfig::Acme {
            account_dir,
            contact,
            update_interval,
        } => {
            let cert_resolver = Arc::new(ResolveServerCert::new());
            return Ok((
                cert_resolver.clone(),
                Arc::new(acme::AcmeRequestCert {
                    cert_order_cache: Arc::new(RwLock::new(HashMap::new())),
                    cert_resolver: cert_resolver,
                    account_dir: account_dir,
                    contact: contact,
                    update_interval: update_interval,
                }),
            ));
        }
    }
}

async fn load_cert(
    certificate_pem: impl AsRef<Path>,
    key_pem: impl AsRef<Path>,
    server_name: Option<&str>,
) -> Result<(CertifiedKey, String, String), Box<dyn std::error::Error + Send + Sync>> {
    if certificate_pem.as_ref().is_file() && key_pem.as_ref().is_file() {
        let key_pem = tokio::fs::read_to_string(key_pem).await?;
        let certificate_pem = tokio::fs::read_to_string(certificate_pem).await?;
        let key = private_key(&mut key_pem.as_bytes())?;
        let cert = certs(&mut certificate_pem.as_bytes()).collect::<Result<Vec<_>, _>>()?;
        let key = key.ok_or_else(|| -> Box<dyn std::error::Error + Send + Sync> {
            if let Some(server_name) = server_name {
                format!("No certificate found for server name \"{}\"", server_name).into()
            } else {
                "Invalid private key".into()
            }
        })?;
        let key = any_supported_type(&key)?;
        return Ok((CertifiedKey::new(cert, key), key_pem, certificate_pem));
    }
    if let Some(server_name) = server_name {
        return Err(format!("No certificate found for server name \"{}\"", server_name).into());
    } else {
        return Err("No certificate found".into());
    }
}
