use super::load_cert;
use crate::cache_mgr::CacheData;
use crate::cache_mgr::CacheMgr;
use crate::handler::Handler;
use async_trait::async_trait;
use rustls::server::ClientHello;
use rustls::{server::ResolvesServerCert, sign::CertifiedKey};
use std::collections::HashMap;
use std::path::Path;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::RwLock;

#[derive(Default)]
pub struct Certs(std::sync::RwLock<HashMap<String, CacheData<CertifiedKey>>>);

impl std::fmt::Debug for Certs {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Certs").finish()
    }
}

impl ResolvesServerCert for Certs {
    fn resolve(&self, client_hello: ClientHello) -> Option<Arc<CertifiedKey>> {
        let server_name = client_hello.server_name()?;
        self.get_cert(server_name)
            .map(|cache_data| Arc::new(cache_data.data.clone()))
    }
}

impl Certs {
    pub fn new() -> Certs {
        Self::default()
    }
    pub fn get_cert(&self, server_name: &str) -> Option<CacheData<CertifiedKey>> {
        self.0.read().unwrap().get(server_name).cloned()
    }
    // pub fn remove_cert_if_expired(&self, server_name: &str) {
    //     let mut certs = self.certs.write().unwrap();
    //     if let Some(cert) = certs.get(server_name) {
    //         if cert.expired() {
    //             certs.remove(server_name);
    //         }
    //     }
    // }
}

#[derive(Clone)]
pub struct MultipleCertHandler {
    pub cert_dir: String,
    pub update_interval: u64,
}

#[async_trait]
impl Handler<String> for MultipleCertHandler {
    type Out = Result<(CertifiedKey, Duration), String>;
    async fn handle(&self, server_name: String) -> Self::Out {
        let dir = Path::new(&self.cert_dir).join(&server_name);
        let certificate_pem = dir.join("fullchain.pem");
        let key_pem = dir.join("privkey.pem");
        let (certified_key, _, _) = load_cert(certificate_pem, key_pem, Some(&server_name))
            .await
            .map_err(|err| err.to_string())?;
        return Ok((certified_key, Duration::from_secs(self.update_interval)));
    }
}

pub struct MultipleRequestCert<H> {
    pub certs: Arc<Certs>,
    pub cert_cache: RwLock<HashMap<String, Arc<CacheMgr<H, String, CertifiedKey, String>>>>,
    pub handler: H,
}

#[async_trait]
impl<H> Handler<Arc<str>> for MultipleRequestCert<H>
where
    H: Handler<String, Out = Result<(CertifiedKey, Duration), String>> + Clone,
{
    type Out = Result<(), Box<dyn std::error::Error + Send + Sync>>;
    async fn handle(&self, server_name: Arc<str>) -> Self::Out {
        if let Some(certified_key) = self.certs.get_cert(&server_name) {
            if certified_key.expired() {
                self.get_cert(&server_name).await?;
            }
        } else {
            self.get_cert(&server_name).await?;
        }
        return Ok(());
    }
}

impl<H> MultipleRequestCert<H>
where
    H: Handler<String, Out = Result<(CertifiedKey, Duration), String>> + Clone,
{
    async fn get_or_create_cache_mgr(
        &self,
        server_name: &str,
    ) -> Arc<CacheMgr<H, String, CertifiedKey, String>> {
        let cert_cache = self.cert_cache.read().await;
        let cert_cache_mgr = cert_cache.get(server_name).map(Clone::clone);
        drop(cert_cache);
        if let Some(cert_cache_mgr) = cert_cache_mgr {
            cert_cache_mgr
        } else {
            let mut cert_cache = self.cert_cache.write().await;
            let cert_cache_mgr = cert_cache
                .entry(server_name.to_string())
                .or_insert_with(|| {
                    Arc::new(CacheMgr::new(self.handler.clone(), server_name.to_string()))
                })
                .clone();
            drop(cert_cache);
            cert_cache_mgr
        }
    }

    async fn get_cert(&self, server_name: &str) -> Result<CacheData<CertifiedKey>, String> {
        let cert_cache_mgr = self.get_or_create_cache_mgr(server_name).await;
        let cache_data = cert_cache_mgr.get_fresh_data_full().await?;
        self.certs
            .0
            .write()
            .unwrap()
            .insert(server_name.to_string(), cache_data.clone());
        return Ok(cache_data);
    }
}
