use super::load_cert;
use crate::cache_mgr::CacheData;
use crate::cache_mgr::CacheMgr;
use crate::handler::Handler;
use async_trait::async_trait;
use rustls::server::ClientHello;
use rustls::{server::ResolvesServerCert, sign::CertifiedKey};
use std::path::Path;
use std::sync::Arc;
use std::time::Duration;

#[derive(Default)]
pub struct Cert(std::sync::RwLock<Option<CacheData<CertifiedKey>>>);

impl std::fmt::Debug for Cert {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Cert").finish()
    }
}

impl ResolvesServerCert for Cert {
    fn resolve(&self, _client_hello: ClientHello) -> Option<Arc<CertifiedKey>> {
        self.get_cert()
            .map(|cache_data| Arc::new(cache_data.data.clone()))
    }
}

impl Cert {
    pub fn new() -> Cert {
        Self::default()
    }
    pub fn get_cert(&self) -> Option<CacheData<CertifiedKey>> {
        self.0.read().unwrap().clone()
    }
}

pub struct SingleCertHandler {
    pub server_cert: String,
    pub server_key: String,
    pub update_interval: u64,
}

#[async_trait]
impl Handler<()> for SingleCertHandler {
    type Out = Result<(CertifiedKey, Duration), String>;
    async fn handle(&self, _: ()) -> Self::Out {
        let certificate_pem = Path::new(&self.server_cert);
        let key_pem = Path::new(&self.server_key);
        let (certified_key, _, _) = load_cert(certificate_pem, key_pem, None)
            .await
            .map_err(|err| err.to_string())?;
        return Ok((certified_key, Duration::from_secs(self.update_interval)));
    }
}

pub struct SingleRequestCert {
    pub cert: Arc<Cert>,
    pub cert_cache_mgr: CacheMgr<SingleCertHandler, (), CertifiedKey, String>,
}

#[async_trait]
impl Handler<Arc<str>> for SingleRequestCert {
    type Out = Result<(), Box<dyn std::error::Error + Send + Sync>>;
    async fn handle(&self, _server_name: Arc<str>) -> Self::Out {
        if let Some(certified_key) = self.cert.get_cert() {
            if certified_key.expired() {
                self.get_cert().await?;
            }
        } else {
            self.get_cert().await?;
        }
        return Ok(());
    }
}

impl SingleRequestCert {
    async fn get_cert(&self) -> Result<CacheData<CertifiedKey>, String> {
        let cache_data = self.cert_cache_mgr.get_fresh_data_full().await?;
        self.cert.0.write().unwrap().replace(cache_data.clone());
        return Ok(cache_data);
    }
}
