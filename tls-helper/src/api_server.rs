use crate::common;
use crate::handler::Handler;
use async_trait::async_trait;
use rustls::crypto::ring::sign::any_supported_type;
use rustls::sign::CertifiedKey;
use rustls_pemfile::certs;
use rustls_pemfile::private_key;
use serde::{Deserialize, Serialize};
use std::time::Duration;

#[derive(Serialize, Deserialize, Debug)]
struct QueryCertReq {
    server_name: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct CertAndKey {
    certificate: String,
    private_key: String,
    timeout: u64,
}

#[derive(Clone)]
pub struct ApiServerCertHandler {
    pub api_url: String,
    pub api_key: Option<String>,
}

#[async_trait]
impl Handler<String> for ApiServerCertHandler {
    type Out = Result<(CertifiedKey, Duration), String>;
    async fn handle(&self, server_name: String) -> Self::Out {
        self.query_cert(server_name)
            .await
            .map_err(|err| err.to_string())
    }
}

impl ApiServerCertHandler {
    async fn query_cert(
        &self,
        server_name: String,
    ) -> Result<(CertifiedKey, Duration), Box<dyn std::error::Error + Send + Sync>> {
        let req_bytes = serde_json::to_vec(&QueryCertReq {
            server_name: server_name.clone(),
        })?;
        let mut builder = reqwest::Client::new().post(&self.api_url);
        if let Some(api_key) = &self.api_key {
            builder = builder.header("X-Api-Key", api_key.clone())
        }
        let response = builder.body(req_bytes).send().await?;
        let status = response.status();
        if status.is_success() {
            let resp_bytes = response.bytes().await?;
            let resp: common::Response<CertAndKey> = serde_json::from_slice(&resp_bytes)?;
            let cert_and_key = common::get_resp_data(resp)?;
            let key = private_key(&mut cert_and_key.private_key.as_bytes())?;
            let cert =
                certs(&mut cert_and_key.certificate.as_bytes()).collect::<Result<Vec<_>, _>>()?;
            let key = key.ok_or_else(|| -> Box<dyn std::error::Error + Send + Sync> {
                format!("Invalid private key for server name \"{}\"", server_name).into()
            })?;
            let key = any_supported_type(&key)?;
            return Ok((
                CertifiedKey::new(cert, key),
                Duration::from_secs(cert_and_key.timeout),
            ));
        } else {
            return Err(format!("Request remote interface failed. Status: {:?}", status).into());
        }
    }
}
