use context::init_config;
use context::init_context;
use log::LevelFilter;
use log4rs;
use log4rs::append::console::ConsoleAppender;
use log4rs::config::{Appender, Root};
use log4rs::encode::pattern::PatternEncoder;
use phantom_protocol::client_proxy::build_common_rpc_client;
use phantom_protocol::Request;

mod base62;
mod context;

pub type GenError = Box<dyn std::error::Error>;

fn init_console_log() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let console = ConsoleAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            "{d(%Y-%m-%d %H:%M:%S)} {l} {t}: {m}{n}",
        )))
        .build();
    let config = log4rs::config::Config::builder()
        .appender(Appender::builder().build("CONSOLE", Box::new(console)))
        .build(Root::builder().appender("CONSOLE").build(LevelFilter::Warn))?;
    log4rs::init_config(config)?;
    return Ok(());
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let config = init_config().map_err(|err| err.to_string())?;
    if let Some(log_cfg_path) = config.log_cfg_path.as_ref() {
        if let Err(err) = log4rs::init_file(log_cfg_path, Default::default()) {
            println!("init log4rs failed, {}", err);
            init_console_log()?;
        }
    } else {
        init_console_log()?;
    }
    let context = init_context(config).await.map_err(|err| err.to_string())?;
    let (client, server_mgr, service) =
        build_common_rpc_client(context.aes_key, context.encrypt_message).await;
    tokio::spawn(service);
    server_mgr
        .add_server(phantom_network_adapter::tokio::ConnectInfo {
            server_addr: context.server_addr,
            heartbeat_config: context.heartbeat_config,
        })
        .await;
    let request = Request {
        uri: "/api/keepalive".into(),
        ip: "127.0.0.1".into(),
        session: None,
        versions: None,
        headers: None,
        body: "hello world".into(),
    };
    let response = client.call_remote(&request, None).await?;
    println!("response session: {:?}", response.session);
    println!("response body: {:?}", response.body);
    return Ok(());
}
