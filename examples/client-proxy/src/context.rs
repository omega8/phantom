use crate::base62;
use bytestring::ByteString;
use json5;
use log;
use serde::{Deserialize, Serialize};
use std::env;
use std::fs::read_to_string;
use std::net::SocketAddr;
use std::time::Duration;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct HeartbeatConfig {
    pub interval: u64,
    pub timeout: u64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub host: String,
    pub port: u16,
    pub aes_key: String,
    pub encrypt_message: bool,
    pub log_cfg_path: Option<String>,
    pub heartbeat: Option<HeartbeatConfig>,
}

pub struct Context {
    pub server_addr: SocketAddr,
    pub heartbeat_config: phantom_network_adapter::HeartbeatConfig,
    pub aes_key: [u8; 32],
    pub encrypt_message: bool,
}

pub fn init_config() -> Result<Config, ByteString> {
    let mut args = Vec::new();
    for v in env::args() {
        args.push(v);
    }
    let cfg_path = args
        .get(1)
        .ok_or_else(|| -> ByteString { ByteString::from_static("app require 1 parameter!") })?;
    let content = read_to_string(&cfg_path).map_err(|err| {
        log::error!("read file to string error: {}", err);
        return ByteString::from_static("read file to string error");
    })?;
    let config: Config = json5::from_str(&content).map_err(|err| -> ByteString {
        log::error!("{:?}", err);
        return ByteString::from_static("配置文件格式错误");
    })?;
    return Ok(config);
}

pub async fn init_context(config: Config) -> Result<Context, ByteString> {
    let heartbeat_config = config
        .heartbeat
        .as_ref()
        .map(|heartbeat| phantom_network_adapter::HeartbeatConfig {
            interval: Duration::from_secs(heartbeat.interval),
            timeout: Duration::from_secs(heartbeat.timeout),
        })
        .unwrap_or_default();
    let aes_key_bytes = base62::decode(&config.aes_key).map_err(|err| -> ByteString {
        log::error!("{:?}", err);
        return ByteString::from_static("通信秘钥格式格式错误");
    })?;
    if 32 != aes_key_bytes.len() {
        return Err(ByteString::from_static("通信秘钥必须是256位"));
    }
    let mut aes_key = [0u8; 32];
    aes_key.copy_from_slice(&aes_key_bytes);
    return Ok(Context {
        server_addr: SocketAddr::new(
            config.host.parse().map_err(|err| -> ByteString {
                log::error!("Host invalid {:?}", err);
                ByteString::from_static("Host invalid!")
            })?,
            config.port,
        ),
        heartbeat_config: heartbeat_config,
        aes_key: aes_key,
        encrypt_message: config.encrypt_message,
    });
}
