use crate::base62;
use bytestring::ByteString;
use json5;
use log;
use phantom_protocol::ConnectParams;
use serde::{Deserialize, Serialize};
use std::env;
use std::fs::read_to_string;
use std::fs::File;
use std::io::Read;
use std::net::SocketAddr;
use std::time::Duration;

#[derive(Serialize, Deserialize, Debug)]
pub struct Tls {
    pub client_cert: String,
    pub client_key: String,
    pub server_ca: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct HeartbeatConfig {
    pub interval: u64,
    pub timeout: u64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub service_version: Option<String>,
    pub host: String,
    pub port: u16,
    pub tls: Option<Tls>,
    pub aes_key: String,
    pub encrypt_message: bool,
    pub log_cfg_path: Option<String>,
    pub heartbeat: Option<HeartbeatConfig>,
}

pub struct Context {
    pub connect_params: ConnectParams,
    pub aes_key: [u8; 32],
    pub encrypt_message: bool,
}

pub fn read_file(file_path: &str) -> Result<Vec<u8>, ByteString> {
    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(file_path) {
        // The `description` method of `io::Error` returns a string that
        // describes the error
        Err(err) => {
            log::error!("read file error: {}", err);
            return Err(ByteString::from_static("read file error"));
        }
        Ok(file) => file,
    };
    // Read the file contents into a string, returns `io::Result<usize>`
    let mut content = Vec::new();
    match file.read_to_end(&mut content) {
        Err(err) => {
            log::error!("read file to string error: {}", err);
            return Err(ByteString::from_static("read file to string error"));
        }
        Ok(_) => (),
    };
    return Ok(content);
}

pub fn init_config() -> Result<Config, ByteString> {
    let mut args = Vec::new();
    for v in env::args() {
        args.push(v);
    }
    let cfg_path = args
        .get(1)
        .ok_or_else(|| -> ByteString { ByteString::from_static("app require 1 parameter!") })?;
    let content = read_to_string(&cfg_path).map_err(|err| {
        log::error!("read file to string error: {}", err);
        return ByteString::from_static("read file to string error");
    })?;
    let config: Config = json5::from_str(&content).map_err(|err| -> ByteString {
        log::error!("{:?}", err);
        return ByteString::from_static("配置文件格式错误");
    })?;
    return Ok(config);
}

pub async fn init_context(config: Config) -> Result<Context, ByteString> {
    let heartbeat_config = config
        .heartbeat
        .as_ref()
        .map(|heartbeat| phantom_network_adapter::HeartbeatConfig {
            interval: Duration::from_secs(heartbeat.interval),
            timeout: Duration::from_secs(heartbeat.timeout),
        })
        .unwrap_or_default();
    let connect_params = if let Some(tls) = config.tls {
        let client_cert = read_file(&tls.client_cert)?;
        let client_key = read_file(&tls.client_key)?;
        let server_ca = read_file(&tls.server_ca)?;
        ConnectParams::Tls(phantom_network_adapter::tokio_rustls::ConnectInfo {
            server_name: config.host,
            server_host: None,
            server_port: config.port,
            client_cert: client_cert,
            client_key: client_key,
            server_ca: server_ca,
            heartbeat_config: heartbeat_config,
        })
    } else {
        ConnectParams::Common(phantom_network_adapter::tokio::ConnectInfo {
            server_addr: SocketAddr::new(
                config.host.parse().map_err(|err| -> ByteString {
                    log::error!("Host invalid {:?}", err);
                    ByteString::from_static("Host invalid!")
                })?,
                config.port,
            ),
            heartbeat_config: heartbeat_config,
        })
    };
    let aes_key_bytes = base62::decode(&config.aes_key).map_err(|err| -> ByteString {
        log::error!("{:?}", err);
        return ByteString::from_static("通信秘钥格式格式错误");
    })?;
    if 32 != aes_key_bytes.len() {
        return Err(ByteString::from_static("通信秘钥必须是256位"));
    }
    let mut aes_key = [0u8; 32];
    aes_key.copy_from_slice(&aes_key_bytes);
    return Ok(Context {
        connect_params: connect_params,
        aes_key: aes_key,
        encrypt_message: config.encrypt_message,
    });
}
