use bytes::Bytes;
use bytestring::ByteString;
use context::init_config;
use context::init_context;
use futures::prelude::sink::SinkExt;
use log::LevelFilter;
use log4rs;
use log4rs::append::console::ConsoleAppender;
use log4rs::config::{Appender, Root};
use log4rs::encode::pattern::PatternEncoder;
use phantom_protocol::client_worker::start_worker_service;
use phantom_protocol::Request;
use phantom_protocol::Response;
use std::time::Duration;
use sysinfo::System;

mod base62;
mod context;

pub type GenError = Box<dyn std::error::Error>;

async fn ability_loop(mut sender: futures::channel::mpsc::UnboundedSender<u8>) {
    loop {
        let sys = System::new_all();
        let processes = sys.processes();
        let cpu_usage = processes
            .iter()
            .map(|(_, process)| process.cpu_usage())
            .fold(0.0, |v1, v2| v1 + v2)
            / processes.len() as f32;
        let mem_usage = sys.used_memory() as f32 / sys.total_memory() as f32;
        let ability = ((1.0 - cpu_usage.max(mem_usage)) * 255.0).floor() as u8;
        if let Err(err) = sender.send(ability).await {
            log::error!("{:?}", err);
        }
        let step = ((ability as f32 / u8::MAX as f32) * 4.0).round() as u64;
        //等待1到5秒继续上报，能力越小等待时间越短
        tokio::time::sleep(Duration::new(step + 1, 0)).await;
    }
}

async fn handler(req: Request) -> Result<Response, ByteString> {
    println!("uri: {:?}", req.uri);
    println!("session: {:?}", req.session);
    println!("body: {:?}", req.body);
    return Ok(Response {
        session: req.session,
        body: Bytes::from_static(b"hello world"),
    });
}

fn init_console_log() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let console = ConsoleAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            "{d(%Y-%m-%d %H:%M:%S)} {l} {t}: {m}{n}",
        )))
        .build();
    let config = log4rs::config::Config::builder()
        .appender(Appender::builder().build("CONSOLE", Box::new(console)))
        .build(Root::builder().appender("CONSOLE").build(LevelFilter::Warn))?;
    log4rs::init_config(config)?;
    return Ok(());
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let config = init_config().map_err(|err| err.to_string())?;
    if let Some(log_cfg_path) = config.log_cfg_path.as_ref() {
        if let Err(err) = log4rs::init_file(log_cfg_path, Default::default()) {
            println!("init log4rs failed, {}", err);
            init_console_log()?;
        }
    } else {
        init_console_log()?;
    }
    let service_version = config.service_version.clone();
    let context = init_context(config).await.map_err(|err| err.to_string())?;
    let (sender, ability_stream) = futures::channel::mpsc::unbounded::<u8>();
    tokio::spawn(async move {
        ability_loop(sender).await;
    });
    start_worker_service(
        context.connect_params,
        handler,
        context.aes_key,
        service_version.map(From::from),
        context.encrypt_message,
        ability_stream,
    )
    .await
}
