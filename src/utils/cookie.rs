use log;
use std::collections::HashMap;

//解析请求的cookie
pub fn parse_cookie(cookie_str: &str) -> HashMap<String, String> {
    let mut cookies: HashMap<String, String> = HashMap::new();
    for item in cookie_str.split(";") {
        let key_val: Vec<&str> = item.split("=").collect();
        match (key_val.get(0), key_val.get(1)) {
            (Some(key), Some(val)) => {
                let key = key.trim().to_string();
                let val = val.trim().to_string();
                if !key.is_empty() {
                    cookies.insert(key, val);
                } else {
                    log::warn!("cookie部分格式有误(key为空): {:?}", item);
                }
            }
            _ => {
                log::warn!("cookie部分格式有误: {:?}", item);
            }
        }
    }
    return cookies;
}

pub struct CookieAttr {
    pub Expires: Option<String>,
    pub Domain: Option<String>,
    pub Path: Option<String>,
    pub Secure: Option<()>,
    pub HttpOnly: Option<()>,
}

impl CookieAttr {
    pub fn empty() -> CookieAttr {
        return CookieAttr {
            Expires: None,
            Domain: None,
            Path: None,
            Secure: None,
            HttpOnly: None,
        };
    }
}

//格式化响应的cookie
pub fn format_cookie(key: &str, val: &str, attr: &CookieAttr) -> String {
    let EqualConnector = "=";
    let ExpiresConnector = "; Expires=";
    let DomainConnector = "; Domain=";
    let PathConnector = "; Path=";
    let SecureConnector = "; Secure";
    let HttpOnlyConnector = "; HttpOnly";
    let mut capacity = key.len() + EqualConnector.len() + val.len();

    match attr.Expires {
        Some(ref Expires) => {
            capacity += ExpiresConnector.len() + Expires.len();
        }
        None => (),
    }
    match attr.Domain {
        Some(ref Domain) => {
            capacity += DomainConnector.len() + Domain.len();
        }
        None => (),
    }
    match attr.Path {
        Some(ref Path) => {
            capacity += PathConnector.len() + Path.len();
        }
        None => (),
    }
    match attr.Secure {
        Some(ref Secure) => {
            capacity += SecureConnector.len();
        }
        None => (),
    }
    match attr.HttpOnly {
        Some(ref HttpOnly) => {
            capacity += HttpOnlyConnector.len();
        }
        None => (),
    }
    let mut output = String::with_capacity(capacity);
    output.push_str(key);
    output.push_str(EqualConnector);
    output.push_str(val);
    match attr.Expires {
        Some(ref Expires) => {
            output.push_str(ExpiresConnector);
            output.push_str(Expires);
        }
        None => (),
    }
    match attr.Domain {
        Some(ref Domain) => {
            output.push_str(DomainConnector);
            output.push_str(Domain);
        }
        None => (),
    }
    match attr.Path {
        Some(ref Path) => {
            output.push_str(PathConnector);
            output.push_str(Path);
        }
        None => (),
    }
    match attr.Secure {
        Some(ref Secure) => {
            output.push_str(SecureConnector);
        }
        None => (),
    }
    match attr.HttpOnly {
        Some(ref HttpOnly) => {
            output.push_str(HttpOnlyConnector);
        }
        None => (),
    }
    //添加调试断言，让初始化容量绝对准确，做到字符串不扩容
    debug_assert_eq!(capacity, output.len());
    return output;
}
