use super::cookie::format_cookie;
use super::cookie::CookieAttr;
use bytestring::ByteString;
use headers::HeaderMap;
use hyper::header::HeaderValue;
use log;

pub const SESSION_KEY: &str = "SESSION";

//设置会话
pub fn set_session(
    headers: &mut HeaderMap<HeaderValue>,
    cipher_session_data: &str,
) -> Result<(), ByteString> {
    let cookie = format_cookie(
        SESSION_KEY,
        cipher_session_data,
        &CookieAttr {
            Path: Some(String::from("/")),
            HttpOnly: Some(()),
            ..CookieAttr::empty()
        },
    );
    let header_value = HeaderValue::from_str(&cookie).map_err(|err| -> ByteString {
        log::error!("生成响应头的值不符合规范: {:?}", err);
        return ByteString::from_static("生成响应头的值不符合规范");
    })?;
    headers.append("Set-Cookie", header_value);
    return Ok(());
}
