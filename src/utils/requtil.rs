use hyper::header::HeaderMap;
use hyper::header::HeaderValue;
use http::request::Parts;
use log;

pub fn get_header<'a, K>(req: &'a Parts, header_name: K) -> Option<&'a str>
where
    K: hyper::header::AsHeaderName,
{
    match req.headers.get(header_name) {
        Some(val) => match val.to_str() {
            Ok(v) => {
                return Some(v);
            }
            Err(err) => {
                log::error!("请求头格式异常: {:?}", err);
            }
        },
        None => (),
    }
    return None;
}

pub fn get_client_ip(headers: &HeaderMap<HeaderValue>) -> Option<&str> {
    match headers.get("X-Forwarded-For") {
        Some(ip_list_val) => match ip_list_val.to_str() {
            Ok(ip_list) => {
                return ip_list.split(",").next();
            }
            Err(err) => {
                log::error!("请求头X-Forwarded-For格式异常: {:?}", err);
            }
        },
        None => (),
    }
    match headers.get("X-Real-IP") {
        Some(ip_val) => match ip_val.to_str() {
            Ok(ip) => {
                return Some(ip);
            }
            Err(err) => {
                log::error!("请求头X-Real-IP格式异常: {:?}", err);
            }
        },
        None => (),
    }
    return None;
}
