use body::Body;
use bytes::Bytes;
use bytestring::ByteString;
use context::init_config;
use context::init_context;
use context::Context;
use context::Protocol;
use futures::future::select_all;
use futures::prelude::sink::SinkExt;
use futures::Future;
use headers::Header;
use headers::{ContentLength, ContentType, HeaderMap, HeaderMapExt, Location};
use http::method::Method;
use http::request::Parts;
use http::uri::Scheme;
use http_body_util::BodyExt;
use https::start_http_server;
use https::start_https_server;
use https::IntoBody;
use https::RequestBody;
use hyper::header;
use hyper::header::HeaderValue;
use hyper::{Request, Response, StatusCode, Uri};
use hyper_staticfile::Static;
use hyper_util::rt::tokio::TokioIo;
use log::LevelFilter;
use log4rs;
use log4rs::append::console::ConsoleAppender;
use log4rs::config::{Appender, Root};
use log4rs::encode::pattern::PatternEncoder;
use phantom_protocol::server_worker::start_worker_service;
use phantom_protocol::BindInfo;
use std::collections::HashMap;
use std::net::SocketAddr;
use std::pin::Pin;
use std::str::FromStr;
use std::sync::Arc;
use std::time::Duration;
use tokio::net::TcpStream;
use utils::base62;
use utils::requtil;
use utils::resputil;

mod body;
mod context;
mod https;
mod utils;
pub type GenError = Box<dyn std::error::Error>;
pub type GenResult<T> = Result<T, GenError>;
pub type BoxBody = http_body_util::combinators::BoxBody<Bytes, std::io::Error>;

/**
 * 移除逐跳首部
 */
fn remove_hop_by_hop_headers(headers: &mut HeaderMap<HeaderValue>) {
    headers.remove(header::PROXY_AUTHENTICATE);
    headers.remove(header::TRANSFER_ENCODING);
    headers.remove(header::UPGRADE);
    if let Some(connection) = headers.remove(header::CONNECTION) {
        match connection.to_str() {
            Ok(connection) => {
                for header in connection.split(",") {
                    let header = header.trim();
                    headers.remove(header);
                }
            }
            Err(err) => {
                log::error!("Connection header cannot transfer to a string, {:?}", err);
            }
        }
    }
}

async fn forward_http(
    mut parts: Parts,
    body: RequestBody,
    upstream: &str,
) -> Result<Response<Body>, Box<dyn std::error::Error + Send + Sync>> {
    let path = match parts.uri.query() {
        Some(query) => [upstream, parts.uri.path(), "?", query].concat(),
        None => [upstream, parts.uri.path()].concat(),
    };
    match Uri::from_str(&path) {
        Ok(uri) => {
            let host = uri
                .host()
                .map(|host| host.to_string())
                .ok_or_else(|| "uri has no host")?;
            let port = uri.port_u16().unwrap_or(80);
            parts.uri = uri;
            remove_hop_by_hop_headers(&mut parts.headers);
            let new_req = Request::from_parts(parts, body.into_body());
            let addr = format!("{}:{}", host, port);
            let stream = TcpStream::connect(addr).await?;
            let io = TokioIo::new(stream);
            let (mut sender, conn) = hyper::client::conn::http1::handshake(io).await?;
            tokio::task::spawn(async move {
                if let Err(err) = conn.await {
                    log::error!("Connection failed: {:?}", err);
                }
            });
            let resp = sender.send_request(new_req).await?;
            let (parts, body) = resp.into_parts();
            let resp = body.collect().await?.to_bytes();
            return Ok(Response::from_parts(parts, Body::from(resp)));
        }
        Err(err) => {
            log::error!("上游服务器地址错误: {}, {:?}", upstream, err);
            let status_code = StatusCode::BAD_GATEWAY;
            let status_text = status_code.canonical_reason().unwrap_or("Bad Gateway");
            let mut response = Response::new(Body::from(status_text));
            *response.status_mut() = status_code;
            response
                .headers_mut()
                .typed_insert(ContentType::text_utf8());
            return Ok(response);
        }
    }
}

fn response_not_found() -> Response<Body> {
    let status_code = StatusCode::NOT_FOUND;
    let status_text = status_code.canonical_reason().unwrap_or("Not Found");
    let mut response = Response::new(Body::from(status_text));
    *response.status_mut() = status_code;
    response
        .headers_mut()
        .typed_insert(ContentType::text_utf8());
    return response;
}

fn response_bad_request() -> Response<Body> {
    let status_code = StatusCode::BAD_REQUEST;
    let status_text = status_code.canonical_reason().unwrap_or("Bad Request");
    let mut response = Response::new(Body::from(status_text));
    *response.status_mut() = status_code;
    response
        .headers_mut()
        .typed_insert(ContentType::text_utf8());
    return response;
}

fn response_too_large() -> Response<Body> {
    let status_code = StatusCode::PAYLOAD_TOO_LARGE;
    let status_text = status_code
        .canonical_reason()
        .unwrap_or("Payload Too Large");
    let mut response = Response::new(Body::from(status_text));
    *response.status_mut() = status_code;
    response
        .headers_mut()
        .typed_insert(ContentType::text_utf8());
    return response;
}

fn response_server_error() -> Response<Body> {
    let status_code = StatusCode::INTERNAL_SERVER_ERROR;
    let status_text = status_code
        .canonical_reason()
        .unwrap_or("Internal Server Error");
    let mut response = Response::new(Body::from(status_text));
    *response.status_mut() = status_code;
    response
        .headers_mut()
        .typed_insert(ContentType::text_utf8());
    return response;
}

async fn forward_static(
    req: Request<()>,
    static_responser: Static,
) -> Result<Response<Body>, Box<dyn std::error::Error + Send + Sync>> {
    return match static_responser.serve(req).await {
        Ok(response) => Ok(response.map(|body| Body::from(body::BoxBody::new(body)))),
        Err(err) => {
            log::error!("{:?}", err);
            Ok(response_not_found())
        }
    };
}

fn gen_redirection_html(url: &str) -> String {
    format!(
        r#"<!DOCTYPE html>
<html>
    <head>
        <title>Location</title>
        <meta http-equiv="refresh" content="0;url={url}">
    </head>
    <body></body>
</html>
"#
    )
}

async fn try_handle_request(
    parts: Parts,
    body: RequestBody,
    remote_addr: SocketAddr,
    context: Arc<Context>,
) -> Result<Response<Body>, Box<dyn std::error::Error + Send + Sync>> {
    if context.force_https {
        if let Some(scheme) = parts.uri.scheme() {
            if &Scheme::HTTP == scheme {
                let mut new_parts = parts.uri.clone().into_parts();
                new_parts.scheme = Some(Scheme::HTTPS);
                let new_uri =
                    Uri::from_parts(new_parts).map_err(|err: http::uri::InvalidUriParts| {
                        log::error!(
                            "Transfer to https protocol failed, uri: {}, error: {:?}",
                            parts.uri,
                            err
                        );
                        err
                    })?;
                let new_uri = new_uri.to_string();
                let redirection_html = gen_redirection_html(&new_uri);
                let status_code = StatusCode::TEMPORARY_REDIRECT;
                let mut response = Response::new(Body::from(redirection_html));
                *response.status_mut() = status_code;
                let new_uri = HeaderValue::from_str(&new_uri)?;
                response.headers_mut().insert(Location::name(), new_uri);
                response.headers_mut().typed_insert(ContentType::html());
                return Ok(response);
            }
        }
    }
    if Method::POST == parts.method {
        if let Some(content_length) = parts.headers.typed_get::<ContentLength>() {
            if content_length.0 > context.body_size.get_limit_by_path(parts.uri.path()) {
                return Ok(response_too_large());
            }
        } else {
            return Ok(response_bad_request());
        }
    }
    let mut cookies = requtil::get_header(&parts, "Cookie").map(utils::cookie::parse_cookie);
    let session = cookies
        .as_mut()
        .map(|cookies| cookies.remove(resputil::SESSION_KEY))
        .flatten();
    let versions = cookies
        .as_mut()
        .map(|cookies| cookies.remove("SERVICE_VERSION"))
        .flatten();
    let versions = versions
        .map(|versions| {
            base62::decode(&versions)
                .map_err(|err| {
                    log::warn!("Service version data invalid {:?}", err);
                    err
                })
                .ok()
        })
        .flatten();
    for http_backend in &context.http_backends {
        if parts.uri.path().starts_with(&http_backend.route) {
            return forward_http(parts, body, &http_backend.target).await;
        }
    }
    if let Some((_route, service_label, client)) =
        find_server_proxy(&context.tcp_backends, parts.uri.path())
    {
        let ip_addr = requtil::get_client_ip(&parts.headers)
            .map(|ip_addr| ip_addr.to_string())
            .unwrap_or_else(|| remote_addr.ip().to_string());
        let uri = parts.uri.path().to_string();
        let headers = if let Some(required_headers) = context.required_headers.as_ref() {
            let mut headers = HashMap::new();
            for header in required_headers {
                let value = requtil::get_header(&parts, header).map(|val| val.to_string());
                headers.insert(header.clone(), value);
            }
            Some(headers)
        } else {
            None
        };
        let body = body::read_body(body.into_body()).await?;
        let versions = if let Some(versions) = versions {
            if versions.is_empty() {
                None
            } else {
                serde_json::from_slice::<HashMap<String, String>>(&versions)
                    .map_err(|err| {
                        log::warn!("Service version data invalid {:?}", err);
                        err
                    })
                    .ok()
            }
        } else {
            None
        };
        match handler(
            phantom_protocol::Request {
                uri: uri.into(),
                ip: ip_addr.into(),
                session: session.map(From::from),
                versions: versions,
                headers: headers,
                body: body,
            },
            service_label,
            client,
        )
        .await
        {
            Ok(resp) => {
                let mut response = Response::new(Body::from(resp.body));
                response.headers_mut().typed_insert(ContentType::json());
                if let Some(session) = resp.session {
                    if let Err(err) = resputil::set_session(response.headers_mut(), &session) {
                        log::error!("Write session to cookie failed: {:?}", err);
                        return Ok(response_server_error());
                    }
                }
                return Ok(response);
            }
            Err(err) => {
                log::error!("Internal Server Erro: {:?}", err);
                return Ok(response_server_error());
            }
        };
    }
    if &Method::GET == parts.method {
        if let Some(static_responser) = context.static_responser.as_ref() {
            return forward_static(Request::from_parts(parts, ()), static_responser.clone()).await;
        } else {
            return Ok(response_not_found());
        }
    } else {
        return Ok(response_not_found());
    }
}

async fn ability_loop(mut sender: futures::channel::mpsc::UnboundedSender<u8>) {
    loop {
        let ability = 255;
        if let Err(err) = sender.send(ability).await {
            log::error!("{:?}", err);
        }
        let step = ((ability as f32 / u8::MAX as f32) * 4.0).round() as u64;
        //等待1到5秒继续上报，能力越小等待时间越短
        tokio::time::sleep(Duration::new(step + 1, 0)).await;
    }
}

fn init_console_log() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let console = ConsoleAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            "{d(%Y-%m-%d %H:%M:%S)} {l} {t}: {m}{n}",
        )))
        .build();
    let config = log4rs::config::Config::builder()
        .appender(Appender::builder().build("CONSOLE", Box::new(console)))
        .build(Root::builder().appender("CONSOLE").build(LevelFilter::Warn))?;
    log4rs::init_config(config)?;
    return Ok(());
}

fn find_server_proxy<'a>(
    tcp_backends: &'a [(String, Option<String>, phantom_protocol::proxy::Client)],
    uri: &str,
) -> Option<&'a (String, Option<String>, phantom_protocol::proxy::Client)> {
    for item in tcp_backends {
        if uri.starts_with(&item.0) {
            return Some(item);
        }
    }
    return None;
}

async fn handler(
    req: phantom_protocol::Request,
    service_label: &Option<String>,
    client: &phantom_protocol::proxy::Client,
) -> Result<phantom_protocol::Response, Box<dyn std::error::Error + Send + Sync>> {
    let mut version: Option<ByteString> = None;
    if let Some(service_label) = service_label.as_ref() {
        //tcp服务后端定义了名字
        if let Some(versions) = req.versions.as_ref() {
            version = versions
                .get(service_label)
                .map(|service_version| service_version.clone().into());
        }
    }
    return client.call_remote(&req, version).await;
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let config = init_config().map_err(|err| err.to_string())?;
    if let Some(log_cfg_path) = config.log_cfg_path.as_ref() {
        if let Err(err) = log4rs::init_file(log_cfg_path, Default::default()) {
            println!("init log4rs failed, {}", err);
            init_console_log()?;
        }
    } else {
        init_console_log()?;
    }
    let heartbeat_config = config
        .heartbeat
        .as_ref()
        .map(|heartbeat| phantom_network_adapter::HeartbeatConfig {
            interval: Duration::from_secs(heartbeat.interval),
            timeout: Duration::from_secs(heartbeat.timeout),
        })
        .unwrap_or_default();
    let context = init_context(config).await.map_err(|err| err.to_string())?;
    let context = Arc::new(context);
    let mut backend_tasks: Vec<
        Pin<Box<dyn Future<Output = Result<(), Box<dyn std::error::Error + Send + Sync>>>>>,
    > = Vec::with_capacity(context.endpoints.len());
    for (bind_addr, protocol) in &context.endpoints {
        let context = context.clone();
        match protocol {
            Protocol::Http => {
                backend_tasks.push(Box::pin(start_http_server(
                    bind_addr.clone(),
                    Arc::new(
                        move |(parts, body, remote_addr): (Parts, RequestBody, SocketAddr)| {
                            let context = context.clone();
                            try_handle_request(parts, body, remote_addr, context)
                        },
                    ),
                )));
            }
            Protocol::Https => {
                let cert_config = context.tls.as_ref().ok_or_else(
                    || -> Box<dyn std::error::Error + Send + Sync> {
                        "Tls config not found!".into()
                    },
                )?;
                backend_tasks.push(Box::pin(start_https_server(
                    bind_addr.clone(),
                    cert_config.clone(),
                    context.enable_h3,
                    Arc::new(
                        move |(parts, body, remote_addr): (Parts, RequestBody, SocketAddr)| {
                            let context = context.clone();
                            try_handle_request(parts, body, remote_addr, context)
                        },
                    ),
                )));
            }
            Protocol::Tcp => {
                let aes_key = context.aes_key;
                let encrypt_message = context.encrypt_message;
                let (sender, ability_stream) = futures::channel::mpsc::unbounded::<u8>();
                let context = context.clone();
                let heartbeat_config = heartbeat_config.clone();
                backend_tasks.push(Box::pin(async move {
                    let (_ret1, ret2) = futures::future::join(
                        ability_loop(sender),
                        start_worker_service(
                            BindInfo::Common(phantom_network_adapter::tokio::BindInfo {
                                bind_addr: bind_addr.clone(),
                                heartbeat_config: heartbeat_config.clone(),
                            }),
                            move |req| {
                                let context = context.clone();
                                async move {
                                    if let Some((_route, service_label, client)) =
                                        find_server_proxy(&context.tcp_backends, &req.uri)
                                    {
                                        handler(req, service_label, client)
                                            .await
                                            .map_err(|err| ByteString::from(err.to_string()))
                                    } else {
                                        Err(ByteString::from_static("no worker"))
                                    }
                                }
                            },
                            aes_key,
                            None,
                            encrypt_message,
                            ability_stream,
                        ),
                    )
                    .await;
                    ret2
                }));
            }
        }
    }
    let (first_result, _, _) = select_all(backend_tasks).await;
    first_result?;
    Ok(())
}
