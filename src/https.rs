use crate::body::Body;
use bytes::Buf;
use bytes::Bytes;
use futures::SinkExt;
use futures::StreamExt;
use h3::error::ErrorLevel;
use h3::server::RequestStream;
use h3_quinn::quinn::crypto::rustls::QuicServerConfig;
use http::request::Parts;
use http_body_util::BodyExt;
use hyper::body::Incoming;
use hyper::header::HeaderValue;
use hyper::service::service_fn;
use hyper::Response;
use hyper_util::rt::tokio::TokioIo;
use hyper_util::{rt::TokioExecutor, server::conn::auto::Builder};
use phantom_tls_helper::acme::is_tls_alpn_challenge;
use phantom_tls_helper::async_acme::acme::ACME_TLS_ALPN_NAME;
use phantom_tls_helper::build_cert_resolver;
use phantom_tls_helper::handler::Handler;
use phantom_tls_helper::CertConfig;
use rustls::crypto::ring::default_provider;
use rustls::server::ResolvesServerCert;
use rustls::ServerConfig;
use std::convert::TryFrom;
use std::io::Read;
use std::net::SocketAddr;
use std::sync::Arc;
use tokio::io::AsyncWriteExt;
use tokio::net::TcpListener;
use tokio_rustls::LazyConfigAcceptor;

pub type BoxBody = http_body_util::combinators::BoxBody<Bytes, std::io::Error>;

pub enum RequestBody {
    Hyper(Incoming),
    H3(RequestStream<h3_quinn::RecvStream, Bytes>),
}

impl IntoBody for RequestBody {
    type Body = Body;
    fn into_body(self) -> Self::Body {
        match self {
            RequestBody::Hyper(body) => body.into_body(),
            RequestBody::H3(body) => body.into_body(),
        }
    }
}

pub trait IntoBody: Send {
    type Body: hyper::body::Body<Data = Bytes, Error = std::io::Error> + Unpin + Send + 'static;
    fn into_body(self) -> Self::Body;
}

impl<S> IntoBody for RequestStream<S, Bytes>
where
    S: h3::quic::RecvStream<Buf = Bytes> + Send + 'static,
{
    type Body = Body;
    fn into_body(mut self) -> Self::Body {
        let (mut sender, receiver) =
            futures::channel::mpsc::channel::<Result<Bytes, std::io::Error>>(1000);
        tokio::spawn(async move {
            loop {
                match self.recv_data().await {
                    Ok(data) => {
                        if let Some(data) = data {
                            let mut buf = Vec::new();
                            match data.reader().read_to_end(&mut buf) {
                                Ok(_) => {
                                    sender.send(Ok(buf.into())).await.ok();
                                }
                                Err(err) => {
                                    sender.send(Err(err)).await.ok();
                                }
                            }
                        } else {
                            break;
                        }
                    }
                    Err(err) => {
                        sender
                            .send(Err(std::io::Error::new(std::io::ErrorKind::Other, err)))
                            .await
                            .ok();
                    }
                }
            }
        });
        return Body::from_bytes_stream(receiver);
    }
}

impl IntoBody for Incoming {
    type Body = Body;
    fn into_body(self) -> Self::Body {
        Body::from(BoxBody::new(self.map_err(|err| {
            std::io::Error::new(std::io::ErrorKind::Other, err)
        })))
    }
}

async fn get_server_config(
    server_name: Arc<str>,
    cert_resolver: Arc<dyn ResolvesServerCert>,
    request_cert: Arc<
        dyn Handler<Arc<str>, Out = Result<(), Box<dyn std::error::Error + Send + Sync>>>,
    >,
) -> Result<ServerConfig, Box<dyn std::error::Error + Send + Sync>> {
    request_cert.handle(server_name).await?;
    let mut tls_config = rustls::ServerConfig::builder_with_provider(Arc::new(default_provider()))
        .with_safe_default_protocol_versions()?
        // .with_safe_default_cipher_suites()
        // .with_safe_default_kx_groups()
        // .with_protocol_versions(&[&rustls::version::TLS13])?
        .with_no_client_auth()
        .with_cert_resolver(cert_resolver);
    tls_config.max_early_data_size = u32::MAX;
    tls_config.alpn_protocols = vec![
        b"h3".into(),
        b"h2".into(),
        b"http/1.1".into(),
        b"http/1.0".into(),
    ];
    return Ok(tls_config);
}

fn get_challenge_server_config(
    cert_resolver: Arc<dyn ResolvesServerCert>,
) -> Result<ServerConfig, Box<dyn std::error::Error + Send + Sync>> {
    let mut tls_config = rustls::ServerConfig::builder_with_provider(Arc::new(default_provider()))
        .with_safe_default_protocol_versions()?
        // .with_safe_default_cipher_suites()
        // .with_safe_default_kx_groups()
        // .with_protocol_versions(&[&rustls::version::TLS13])?
        .with_no_client_auth()
        .with_cert_resolver(cert_resolver);
    tls_config.alpn_protocols.push(ACME_TLS_ALPN_NAME.to_vec());
    return Ok(tls_config);
}

fn get_h3_server_config(
    cert_resolver: Arc<dyn ResolvesServerCert>,
) -> Result<ServerConfig, Box<dyn std::error::Error + Send + Sync>> {
    let mut tls_config = rustls::ServerConfig::builder_with_provider(Arc::new(default_provider()))
        .with_safe_default_protocol_versions()?
        // .with_safe_default_cipher_suites()
        // .with_safe_default_kx_groups()
        // .with_protocol_versions(&[&rustls::version::TLS13])?
        .with_no_client_auth()
        .with_cert_resolver(cert_resolver.clone());
    tls_config.max_early_data_size = u32::MAX;
    tls_config.alpn_protocols = vec![
        b"h3".into(),
        b"h2".into(),
        b"http/1.1".into(),
        b"http/1.0".into(),
    ];
    return Ok(tls_config);
}

async fn try_send_response(
    mut sender: RequestStream<h3_quinn::SendStream<Bytes>, Bytes>,
    response: Response<Body>,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let (parts, mut body) = response.into_parts();
    sender
        .send_response(Response::from_parts(parts, ()))
        .await?;
    let mut stream = futures::stream::poll_fn(
        move |cx| -> std::task::Poll<Option<Result<hyper::body::Frame<Bytes>, std::io::Error>>> {
            hyper::body::Body::poll_frame(std::pin::Pin::new(&mut body), cx)
        },
    );
    while let Some(frame) = stream.next().await {
        if let Ok(frame) = frame {
            if let Some(data) = frame.data_ref() {
                sender.send_data(data.clone()).await?;
            }
        }
    }
    sender.finish().await?;
    return Ok(());
}

async fn handle_request(
    h3_header_value: Option<HeaderValue>,
    parts: Parts,
    body: RequestBody,
    remote_addr: SocketAddr,
    handler: Arc<
        dyn Handler<
            (Parts, RequestBody, SocketAddr),
            Out = Result<Response<Body>, Box<dyn std::error::Error + Send + Sync>>,
        >,
    >,
) -> Result<Response<Body>, Box<dyn std::error::Error + Send + Sync>> {
    let mut resp = handler.handle((parts, body, remote_addr)).await?;
    if let Some(h3_header_value) = h3_header_value {
        resp.headers_mut().append("Alt-Svc", h3_header_value);
    }
    return Ok(resp);
}

pub async fn start_http_server(
    addr: SocketAddr,
    handler: Arc<
        dyn Handler<
            (Parts, RequestBody, SocketAddr),
            Out = Result<Response<Body>, Box<dyn std::error::Error + Send + Sync>>,
        >,
    >,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let listener = TcpListener::bind(addr).await?;
    let actual_addr = listener.local_addr()?;
    log::info!("Listening on http://{}", actual_addr);
    println!("Listening on http://{}", actual_addr);
    loop {
        let (stream, remote_addr) = listener.accept().await?;
        let io = TokioIo::new(stream);
        let handler = handler.clone();
        tokio::task::spawn(async move {
            if let Err(err) = Builder::new(TokioExecutor::new())
                .serve_connection(
                    io,
                    service_fn(move |req| {
                        let handler = handler.clone();
                        let (parts, body) = req.into_parts();
                        handle_request(
                            None,
                            parts,
                            RequestBody::Hyper(body),
                            remote_addr,
                            handler.clone(),
                        )
                    }),
                )
                .await
            {
                eprintln!("Error serving connection: {:?}", err);
            }
        });
    }
}

pub async fn start_https_server(
    listen: SocketAddr,
    cert_config: CertConfig,
    h3: bool,
    handler: Arc<
        dyn Handler<
            (Parts, RequestBody, SocketAddr),
            Out = Result<Response<Body>, Box<dyn std::error::Error + Send + Sync>>,
        >,
    >,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let (cert_resolver, request_cert) = build_cert_resolver(cert_config)?;
    let tls_config = get_h3_server_config(cert_resolver.clone())?;
    let port = listen.port();
    let h3_header_value = if h3 {
        Some(HeaderValue::from_str(&format!("h3=\":{port}\"; ma=3600"))?)
    } else {
        None
    };
    let listener = TcpListener::bind(listen).await?;
    let h2_future = async {
        loop {
            match listener.accept().await {
                Ok((stream, remote_addr)) => {
                    let acceptor =
                        LazyConfigAcceptor::new(rustls::server::Acceptor::default(), stream);
                    let handler = handler.clone();
                    let h3_header_value = h3_header_value.clone();
                    let cert_resolver = cert_resolver.clone();
                    let request_cert = request_cert.clone();
                    tokio::spawn(async move {
                        match acceptor.await {
                            Ok(start_handshake) => {
                                let client_hello = start_handshake.client_hello();
                                if let Some(server_name) = client_hello.server_name() {
                                    if is_tls_alpn_challenge(&client_hello) {
                                        log::info!(
                                            "Received TLS-ALPN-01 validation request for server name \"{}\"",
                                            server_name
                                        );
                                        match get_challenge_server_config(cert_resolver.clone()) {
                                            Ok(server_config) => {
                                                match start_handshake
                                                    .into_stream(Arc::new(server_config))
                                                    .await
                                                {
                                                    Ok(mut tls) => {
                                                        if let Err(err) = tls.shutdown().await {
                                                            log::info!(
                                                                "Shutdown challenge tls connection failed: {:?}",
                                                                err
                                                            );
                                                        }
                                                    }
                                                    Err(err) => {
                                                        log::info!(
                                                            "Response challenge tls request failed: {:?}",
                                                            err
                                                        );
                                                    }
                                                }
                                            }
                                            Err(err) => {
                                                log::error!(
                                                    "Choose challenge tls config failed: {:?}",
                                                    err
                                                );
                                            }
                                        }
                                    } else {
                                        match get_server_config(
                                            server_name.to_string().into(),
                                            cert_resolver.clone(),
                                            request_cert.clone(),
                                        )
                                        .await
                                        {
                                            Ok(server_config) => {
                                                match start_handshake
                                                    .into_stream(Arc::new(server_config))
                                                    .await
                                                {
                                                    Ok(stream) => {
                                                        let io = TokioIo::new(stream);
                                                        if let Err(err) =
                                                            Builder::new(TokioExecutor::new())
                                                                .serve_connection(
                                                                    io,
                                                                    service_fn(move |req| {
                                                                        let (parts, body) =
                                                                            req.into_parts();
                                                                        handle_request(
                                                                            h3_header_value.clone(),
                                                                            parts,
                                                                            RequestBody::Hyper(
                                                                                body,
                                                                            ),
                                                                            remote_addr,
                                                                            handler.clone(),
                                                                        )
                                                                    }),
                                                                )
                                                                .await
                                                        {
                                                            log::error!(
                                                                "Error serving tls connection: {:?}",
                                                                err
                                                            );
                                                        }
                                                    }
                                                    Err(err) => {
                                                        log::error!(
                                                            "Response tls request failed: {:?}",
                                                            err
                                                        );
                                                    }
                                                }
                                            }
                                            Err(err) => {
                                                log::error!("Choose tls config failed: {:?}", err);
                                            }
                                        }
                                    }
                                } else {
                                    log::info!("Server name of tls connection is empty");
                                }
                            }
                            Err(err) => {
                                log::info!("Tls accept error: {:?}", err);
                            }
                        }
                    });
                }
                Err(err) => {
                    log::info!("Tcp accept error: {:?}", err);
                }
            }
        }
    };
    if h3 {
        let server_config =
            quinn::ServerConfig::with_crypto(Arc::new(QuicServerConfig::try_from(tls_config)?));
        let endpoint = quinn::Endpoint::server(server_config, listen)?;
        log::info!("Listening on https://{}", listen);
        println!("Listening on https://{}", listen);
        futures::future::join(h2_future, async {
            while let Some(new_conn) = endpoint.accept().await {
                let handler = handler.clone();
                tokio::spawn(async move {
                    match new_conn.await {
                        Ok(conn) => {
                            let remote_addr = conn.remote_address();
                            log::info!("new connection established");
                            let mut h3_conn: h3::server::Connection<h3_quinn::Connection, Bytes> =
                                h3::server::Connection::new(h3_quinn::Connection::new(conn))
                                    .await
                                    .unwrap();
                            loop {
                                match h3_conn.accept().await {
                                    Ok(Some((req, stream))) => {
                                        let handler = handler.clone();
                                        let remote_addr = remote_addr.clone();
                                        tokio::spawn(async move {
                                            let (parts, _) = req.into_parts();
                                            let (sender, reciver) = stream.split();
                                            match handle_request(
                                                None,
                                                parts,
                                                RequestBody::H3(reciver),
                                                remote_addr,
                                                handler.clone(),
                                            )
                                            .await
                                            {
                                                Ok(resp) => {
                                                    if let Err(err) =
                                                        try_send_response(sender, resp).await
                                                    {
                                                        log::error!(
                                                            "send response failed: {}",
                                                            err
                                                        );
                                                    }
                                                }
                                                Err(err) => {
                                                    log::error!("handling request failed: {}", err);
                                                }
                                            }
                                        });
                                    }
                                    Ok(None) => {
                                        break;
                                    }
                                    Err(err) => {
                                        log::info!("error on accept {}", err);
                                        match err.get_error_level() {
                                            ErrorLevel::ConnectionError => break,
                                            ErrorLevel::StreamError => continue,
                                        }
                                    }
                                }
                            }
                        }
                        Err(err) => {
                            log::info!("accepting connection failed: {:?}", err);
                        }
                    }
                });
            }
            endpoint.wait_idle().await;
        })
        .await;
    } else {
        log::info!("Listening on https://{}", listen);
        println!("Listening on https://{}", listen);
        h2_future.await;
    }
    return Ok(());
}
