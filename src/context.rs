use crate::utils::base62;
use bytestring::ByteString;
use hyper_staticfile::Static;
use json5;
use log;
use phantom_protocol::proxy::Client;
use phantom_protocol::server_proxy::start_proxy_service;
use phantom_protocol::BindInfo;
use phantom_tls_helper::CertConfig;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::env;
use std::fs::read_to_string;
use std::net::IpAddr;
use std::net::Ipv4Addr;
use std::net::SocketAddr;
use std::path::Path;
use std::time::Duration;

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
#[serde(rename_all = "snake_case")]
pub enum Protocol {
    Http,
    Https,
    Tcp,
}

impl Protocol {
    pub fn default_port(&self) -> u16 {
        match self {
            Protocol::Http => 80,
            Protocol::Https => 443,
            Protocol::Tcp => 8888,
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TcpListener {
    pub host: IpAddr,
    pub port: u16,
    pub tls: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct HttpBackend {
    pub route: String,
    pub target: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TcpBackend {
    pub route: String,
    pub service_label: Option<String>,
    pub listen: TcpListener,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Tls {
    pub server_cert: String,
    pub server_key: String,
    pub client_ca: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct HeartbeatConfig {
    pub interval: u64,
    pub timeout: u64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PathBodySize {
    pub limit: u64,
    pub path: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct BodySizeConfig {
    pub limit: u64,
    pub api: Option<Vec<PathBodySize>>,
}

pub struct BodySize {
    pub limit: u64,
    pub api: HashMap<String, u64>,
}

impl Default for BodySize {
    fn default() -> Self {
        BodySize {
            limit: 1024 * 1024,
            api: HashMap::new(),
        }
    }
}

impl From<BodySizeConfig> for BodySize {
    fn from(conf: BodySizeConfig) -> Self {
        let mut map = HashMap::new();
        if let Some(api) = conf.api {
            for item in api {
                map.insert(item.path, item.limit);
            }
        }
        BodySize {
            limit: conf.limit,
            api: map,
        }
    }
}

impl BodySize {
    pub fn get_limit_by_path(&self, path: &str) -> u64 {
        return self.api.get(path).map(|v| *v).unwrap_or(self.limit);
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Endpoint {
    pub host: Option<IpAddr>,
    pub port: Option<u16>,
    pub protocol: Protocol,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub endpoints: Vec<Endpoint>,
    pub force_https: Option<bool>,
    pub enable_h3: Option<bool>,
    pub log_cfg_path: Option<String>,
    pub root: Option<String>,
    pub body_size: Option<BodySizeConfig>,
    pub http_backends: Option<Vec<HttpBackend>>,
    pub tcp_backends: Option<Vec<TcpBackend>>,
    pub required_headers: Option<Vec<String>>,
    pub tls: Option<CertConfig>,
    pub aes_key: String,
    pub encrypt_message: bool,
    pub heartbeat: Option<HeartbeatConfig>,
}

pub struct Context {
    pub endpoints: Vec<(SocketAddr, Protocol)>,
    pub force_https: bool,
    pub enable_h3: bool,
    pub static_responser: Option<Static>,
    pub body_size: BodySize,
    pub http_backends: Vec<HttpBackend>,
    pub tcp_backends: Vec<(String, Option<String>, Client)>,
    pub required_headers: Option<Vec<String>>,
    pub tls: Option<CertConfig>,
    pub aes_key: [u8; 32],
    pub encrypt_message: bool,
}

pub fn init_config() -> Result<Config, ByteString> {
    let mut args = Vec::new();
    for v in env::args() {
        args.push(v);
    }
    let cfg_path = args
        .get(1)
        .ok_or_else(|| -> ByteString { ByteString::from_static("app require 1 parameter!") })?;
    let content = read_to_string(&cfg_path).map_err(|err| {
        log::error!("read file to string error: {}", err);
        return ByteString::from_static("read file to string error");
    })?;
    let config: Config = json5::from_str(&content).map_err(|err| -> ByteString {
        log::error!("{:?}", err);
        return ByteString::from(format!("配置文件格式错误: {}", err));
    })?;
    return Ok(config);
}

pub async fn init_context(config: Config) -> Result<Context, ByteString> {
    let mut endpoints = Vec::with_capacity(config.endpoints.len());
    for endpoint in config.endpoints {
        let addr = SocketAddr::new(
            endpoint.host.unwrap_or(IpAddr::V4(Ipv4Addr::UNSPECIFIED)),
            endpoint.port.unwrap_or(endpoint.protocol.default_port()),
        );
        endpoints.push((addr, endpoint.protocol));
    }
    let aes_key_bytes = base62::decode(&config.aes_key).map_err(|err| -> ByteString {
        log::error!("{:?}", err);
        return ByteString::from_static("通信秘钥格式格式错误");
    })?;
    if 32 != aes_key_bytes.len() {
        return Err(ByteString::from_static("通信秘钥必须是256位"));
    }
    let mut aes_key = [0u8; 32];
    aes_key.copy_from_slice(&aes_key_bytes);
    let mut tcp_backends: Vec<(SocketAddr, String, Option<String>, Client)> = Vec::new();
    if let Some(backends) = config.tcp_backends {
        for item in backends {
            let bind_addr = SocketAddr::new(item.listen.host, item.listen.port);
            let mut client_opt: Option<Client> = None;
            for (addr, _route, _service_label, client) in &tcp_backends {
                if *addr == bind_addr {
                    client_opt.replace(client.clone());
                    break;
                }
            }
            if let Some(client) = client_opt {
                tcp_backends.push((bind_addr, item.route, item.service_label, client));
            } else {
                let heartbeat_config = config
                    .heartbeat
                    .as_ref()
                    .map(|heartbeat| phantom_network_adapter::HeartbeatConfig {
                        interval: Duration::from_secs(heartbeat.interval),
                        timeout: Duration::from_secs(heartbeat.timeout),
                    })
                    .unwrap_or_default();
                let client = if item.listen.tls {
                    let cert_config = config
                        .tls
                        .as_ref()
                        .ok_or_else(|| ByteString::from_static("Tls config not found!"))?;
                    let client = start_proxy_service(
                        BindInfo::Tls(phantom_network_adapter::tokio_rustls::BindInfo {
                            bind_addr: bind_addr,
                            cert_config: cert_config.clone(),
                            heartbeat_config: heartbeat_config,
                        }),
                        aes_key,
                        config.encrypt_message,
                    )
                    .await?;
                    client
                } else {
                    let client = start_proxy_service(
                        BindInfo::Common(phantom_network_adapter::tokio::BindInfo {
                            bind_addr: bind_addr,
                            heartbeat_config: heartbeat_config,
                        }),
                        aes_key,
                        config.encrypt_message,
                    )
                    .await?;
                    client
                };
                tcp_backends.push((bind_addr, item.route, item.service_label.clone(), client));
            }
        }
    }
    let context = Context {
        endpoints: endpoints,
        force_https: config.force_https.unwrap_or_default(),
        enable_h3: config.enable_h3.unwrap_or_default(),
        static_responser: config.root.map(|path| Static::new(Path::new(&path))),
        body_size: config.body_size.map(From::from).unwrap_or_default(),
        http_backends: config.http_backends.unwrap_or_else(|| Vec::new()),
        tcp_backends: tcp_backends
            .into_iter()
            .map(|(_addr, route, service_label, client)| (route, service_label, client))
            .collect(),
        required_headers: config.required_headers,
        tls: config.tls,
        aes_key: aes_key,
        encrypt_message: config.encrypt_message,
    };
    return Ok(context);
}
